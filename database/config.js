module.exports = {
    MODELS_URL: process.env.MODELS_URL || __dirname + '/models/*.js',
    Operations: {
        read: 0,
        add: 1,
        edit: 2,
        remove: 3
    }
};