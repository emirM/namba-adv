const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
   name: { type: String, required: true },
   email: { type: String },
   phone: { type: String },
   password: { type: String },
   isAdmin: { type: Boolean, default: false },
   roleId: { type: Schema.Types.ObjectId, ref: 'Role', required: true }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('User', schema); // User
module.exports = (registry) => {
    registry['User'] = Model;
};
