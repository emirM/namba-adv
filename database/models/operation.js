const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
   name: { type: String, required: true },
   modelLabel: { type: String, required: true },
   isRead: { type: Boolean, default: true },
   isAdd: { type: Boolean, default: false },
   isRemove: { type: Boolean, default: false },
   isEdit: { type: Boolean, default: false },
   roleId: { type: Schema.Types.ObjectId, ref: 'Role', required: true }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('Operation', schema); // Операции
module.exports = (registry) => {
    registry['Operation'] = Model;
};
