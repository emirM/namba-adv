const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
    name: { type: String, required: true },
    tariffs: { type: Schema.Types.Mixed },
    coefficients: { type: Schema.Types.Mixed }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('TVChannel', schema); // TVChannel
module.exports = (registry) => {
    registry['TVChannel'] = Model;
};
