const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
    name: { type: String, default: 'Мастер Ши Фу' },
    address: { type: String, default: 'Ак Ордо' },
    inn: { type: String },
    bankDetails: { type: String }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('Client', schema); // Блоки
module.exports = (registry) => {
    registry['Client'] = Model;
};
