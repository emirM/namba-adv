const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
    name: { type: String, required: true },
    orders: [{ type: Schema.Types.ObjectId, ref: 'Order' }],
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('OrderShablon', schema); // Блоки
module.exports = (registry) => {
    registry['OrderShablon'] = Model;
};
