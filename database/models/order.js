const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
   categoryId: { type: Schema.Types.ObjectId, ref: 'CategoryProduct', required: true },
   productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
   userId: { type: Schema.Types.ObjectId, ref: 'User' },
   clientId: { type: Schema.Types.ObjectId, ref: 'Client' },
   calc: { type: Schema.Types.Mixed, required: true }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('Order', schema); // Order
module.exports = (registry) => {
    registry['Order'] = Model;
};
