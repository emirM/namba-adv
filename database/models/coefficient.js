const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
    name: { type: String, required: true },
    tvChannelId: { type: Schema.Types.ObjectId, ref: 'TVChannel', required: true },
    value: { type: Number, default: 0 }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('Сoefficient', schema); // Блоки
module.exports = (registry) => {
    registry['Сoefficient'] = Model;
};
