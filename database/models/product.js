const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
   name: { type: String, required: true },
   calcId: { type: Schema.Types.ObjectId, ref: 'Calc', required: true },
   categoryId: { type: Schema.Types.ObjectId, ref: 'CategoryProduct', required: true }
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('Product', schema); // Product
module.exports = (registry) => {
    registry['Product'] = Model;
};
