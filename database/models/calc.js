const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

let schema = new Schema({
    name: { type: Schema.Types.String, required: true },
    options: [{
        name: { type: Schema.Types.String, default: 'поле без имени' },
        value: { type: Schema.Types.Mixed, default: 0 },
        ref: { type: Schema.Types.String },
        formulas: [{ type: Schema.Types.String }]
    }]
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

const Model = mongoose.model('Calc', schema); // Calculator
module.exports = (registry) => {
    registry['Calc'] = Model;
};
