module.exports = {
    dev: {
       ROOT_DIR: __dirname,
       DB_URL: process.env.DB_URL || 'mongodb://localhost/nambaAdv',
       UPLOAD_DIR: __dirname + '/public/uploads',
       secret: 'sjdhfi*72^%62kMsdloa-',
       superSecretKey: 'sj12d12dsKopf#i341ssd@^%62kMsdlweWeaCoa-',
       STATIC_DIR: __dirname + '/public',
       operationNames: {
           isRead: 'isRead',
           isAdd: 'isAdd',
           isRemove: 'isRemove',
           isEdit: 'isEdit'
       },
       Models: {
           role: {
               modelName: 'Role',
               name: 'Роли'
           },
           operation: {
               modelName: 'Operation',
               name: 'Операции'
           },
           user: {
               modelName: 'User',
               name: 'Пользователи'
           },
           categoryProduct: {
               modelName: 'CategoryProduct',
               name: 'Категория'
           },
           product: {
               modelName: 'Product',
               name: 'Продукт',
               refCalc: true
           },
           tvChannel: {
               modelName: 'TVChannel',
               name: 'Телеканал',
               refCalc: true
           },
           calc: {
               modelName: 'Calc',
               name: 'Калькулятор'
           },
           order: {
               modelName: 'Order',
               name: 'Заказ'
           },
           orderShablon: {
               modelName: 'OrderShablon',
               name: 'Шаблоны'
           },
           client: {
               modelName: 'Client',
               name: 'Клиент'
           }
       }
    },
    prod: {
        ROOT_DIR: __dirname,
        DB_URL: process.env.DB_URL || 'mongodb://localhost/nambaAdv',
        UPLOAD_DIR: __dirname + '/public/uploads',
        secret: 'sjdhfi*72^%62kMsdloa-',
        superSecretKey: 'sj12d12dsKopf#i341ssd@^%62kMsdlweWeaCoa-',
        STATIC_DIR: __dirname + '/public',
        operationNames: {
            isRead: 'isRead',
            isAdd: 'isAdd',
            isRemove: 'isRemove',
            isEdit: 'isEdit'
        },
        Models: {
            role: {
                modelName: 'Role',
                name: 'Роли'
            },
            operation: {
                modelName: 'Operation',
                name: 'Операции'
            },
            user: {
                modelName: 'User',
                name: 'Пользователи',
            },
            categoryProduct: {
                modelName: 'CategoryProduct',
                name: 'Категория'
            },
            product: {
                modelName: 'Product',
                name: 'Продукт'
            },
            tarrif: {
                modelName: 'Tariff',
                name: 'Тариф',
                refCalc: true
            },
            tarrif: {
                modelName: 'Tariff',
                name: 'Тариф',
                refCalc: true
            },
            tvChannel: {
                modelName: 'TVChannel',
                name: 'Телеканал',
                refCalc: true
            },
            calc: {
                modelName: 'Calc',
                name: 'Калькулятор'
            },
            order: {
                modelName: 'Order',
                name: 'Заказ'
            },
            orderShablon: {
                modelName: 'OrderShablon',
                name: 'Шаблоны'
            },
            client: {
                modelName: 'Client',
                name: 'Клиент'
            }
        }
     }
}