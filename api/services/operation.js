const env = process.env.NODE_ENV || 'prod';
const config = require('../config')[env];
module.exports = {
    async addDefaultOperations(db, roleId) {
        console.log('addDefaultOperations');
        if (!db || !roleId) {
            console.log('data not valid');
            return;
        }
        let models = Object.keys(config.Models);
        if (!models || models.length < 1) {
            console.log('models is null');
            return;
        }
        let modelsSave = [];
        models.forEach((key) => {
            let model = new db.Operation({
                modelLabel: config.Models[key].modelName,
                name: config.Models[key].name,
                roleId: roleId
            });
            modelsSave.push(model.save());
        });
        return await Promise.all(modelsSave);
    }
}