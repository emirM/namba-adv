module.exports = (app) => {
    const db = app.get('db');
    const masterQuery = {};

    masterQuery.add = async (modelName, data) => {
        if (!modelName || !data) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        let newModel = await new model(data).save();
        return newModel;
    };

    masterQuery.edit = async (modelName, data) => {
        if (!modelName || !data || !data._id) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        return await model.findOneAndUpdate({_id: data._id}, data);
    };

    masterQuery.getAll = async function (modelName, data) {
        data = data || {};
        if (!modelName) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }

        const textSearch = data.textSearch;
        let query = data.query || {};
        const options = {
            page: parseInt(data.page) || 1,
            limit: parseInt(data.limit) || 30,
        };

        if (data.populate) {
            // { path: 'responsibleUser', select: 'first_name last_name' };
            // [{ path: 'city'}, { path: 'interests'}]
            options.populate = data.populate;
        }
        if (data.sort) {
            options.sort = data.sort;
        }

        /*if (textSearch) {
            const regex = new RegExp('^' + textSearch, 'i'); // ищет только начальное совпадение
            const regex2 = new RegExp('\\ ' + textSearch, 'i'); // ищет слова с пробелСлова = \ слова/i
            query = {
                $or: [
                    { name: regex },
                    { name: regex2 }
                ]
            }
        }*/
        
        return await model.paginate(query, options);
    };

    masterQuery.groupBy = async function(modelName, data) {
        data = data || {};
        console.log(data);
        if (!modelName) {
            console.log('modelName is null');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.log('Model ' + modelName + ' not found.');
            return;
        }
        if (!data.group) {
            console.log('data.group is null');
            return;
        }
        let aggregate = model.aggregate();
        if (data.lookup) {
            aggregate.lookup(data.lookup);
        }
        aggregate.group(data.group); //{_id: '$cityId', date: {$last: "$createdAt"}, count: { $sum: 1 }}
        let options = { page: data.page ? data.page : 1, limit: data.limit ? data.limit : 30 };

        return await model.aggregatePaginate(aggregate, options);
    };

    masterQuery.remove = async function (modelName, data) {
        if (!modelName || !data || !data._id) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        return await model.findByIdAndRemove(data._id);
    };

    masterQuery.removeAll = async function (modelName, data) {
        if (!modelName || !data || !data.query) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        return await model.remove(data.query);
    };

    masterQuery.getBy = async function (modelName, query) {
        if (!modelName || !query) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        return await model.find(query);
    };

    masterQuery.getOneBy = async function (modelName, data) {
        if (!modelName || !data || !data.query) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        //data.populate=[{ path: 'cityId', select: 'name _id' }, { path: 'rangeId', select: 'name _id' }];
        if (data.populate) {
            return await model.findOne(data.query).populate(data.populate);
        }
        return await model.findOne(data.query);
    };

    masterQuery.getLast = async function (modelName, data) {
        if (!modelName || !data || !data.sort) {
            console.error('Parameters not valid.');
            return;
        }
        const model = db[modelName];
        if (!model) {
            console.error('Model ' + modelName + ' not found.');
            return;
        }
        let query = data.query || {};
        if (data.populate) {
            return await model.find(query).sort(data.sort).limit(1).populate(data.populate);
        }
        return await model.find(query).sort(data.sort).limit(1);
    };

    app.set('masterQuery', masterQuery);
};
