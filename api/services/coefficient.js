const env = process.env.NODE_ENV || 'prod';
const config = require('../config')[env];

module.exports = {
    async initCoeficient(MasterQuery, orderId) {
        if (!MasterQuery) {
            console.log('masterQuery is not defined');
            return;
        }
        if (!orderId) {
            console.log('orderId is null');
            return;
        }
        let findCoefficientFixPrice= await MasterQuery.findOne(config.Models.coefficient.modelName, {
            query: { name: 'Фиксированная цена', orderId: orderId }
        });
        if (!findCoefficientFixPrice) {
            MasterQuery.add(config.Models.coefficient.modelName, { name: 'Фиксированная цена', orderId: orderId });
        }
        let findCoefficientFirstBlock= await MasterQuery.findOne(config.Models.coefficient.modelName, {
            query: { name: 'Первый в блоке', orderId: orderId}
        });
        if (!findCoefficientFirstBlock) {
            MasterQuery.add(config.Models.coefficient.modelName, { name: 'Первый в блоке', orderId: orderId });
        }
        let findCoefficientLastBlock= await MasterQuery.findOne(config.Models.coefficient.modelName, {
            query: { name: 'Последний в блоке', orderId: orderId }
        });
        if (!findCoefficientLastBlock) {
            MasterQuery.add(config.Models.coefficient.modelName, { name: 'Последний в блоке', orderId: orderId });
        }
        let findCoefficientMoreBrands= await MasterQuery.findOne(config.Models.coefficient.modelName, {
            query: { name: 'Присутствие двух и более брендов', orderId: orderId }
        });
        if (!findCoefficientMoreBrands) {
            MasterQuery.add(config.Models.coefficient.modelName, { name: 'Присутствие двух и более брендов', orderId: orderId });
        }
        let findCoefficientWithoutPriority= await MasterQuery.findOne(config.Models.coefficient.modelName, {
            query: { name: 'Без приоритета', orderId: orderId }
        });
        if (!findCoefficientWithoutPriority) {
            MasterQuery.add(config.Models.coefficient.modelName, { name: 'Без приоритета', orderId: orderId });
        }
    }
}
