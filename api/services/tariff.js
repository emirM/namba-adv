const env = process.env.NODE_ENV || 'prod';
const config = require('../config')[env];

module.exports = {
    async initTariff(MasterQuery, orderId) {
        if (!MasterQuery) {
            console.log('masterQuery is null');
            return;
        }
        if (!orderId) {
            console.log('orderId is null');
            return;
        }
        let findTariffOfftimeWeekDay = await MasterQuery.findOne(config.Models.tariff.modelName, {
            query: { name: 'Off time (будние дни)', orderId: orderId }
        });
        if (!findTariffOfftimeWeekDay) {
            MasterQuery.add(config.Models.tariff.modelName, { name: 'Off time (будние дни)', orderId: orderId });
        }
        let findTariffOfftimeWeekend = await MasterQuery.findOne(config.Models.tariff.modelName, {
            query: { name: 'Off time (выходные дни)', orderId: orderId }
        });
        if (!findTariffOfftimeWeekDay) {
            MasterQuery.add(config.Models.tariff.modelName, { name: 'Off time (выходные дни)', orderId: orderId });
        }
        let findTariffPrePrimeTime= await MasterQuery.findOne(config.Models.tariff.modelName, {
            query: { name: 'Pre prime time', orderId: orderId }
        });
        if (!findTariffPrePrimeTime) {
            MasterQuery.add(config.Models.tariff.modelName, { name: 'Pre prime time', orderId: orderId });
        }
        let findTariffPrimeTimeWeekday= await MasterQuery.findOne(config.Models.tariff.modelName, {
            query: { name: 'Prime time (будние дни)', orderId: orderId }
        });
        if (!findTariffPrimeTimeWeekday) {
            MasterQuery.add(config.Models.tariff.modelName, { name: 'Prime time (будние дни)', orderId: orderId });
        }
        let findTariffPrimeTimeWeekend= await MasterQuery.findOne(config.Models.tariff.modelName, {
            query: { name: 'Prime time (выходные дни)', orderId: orderId }
        });
        if (!findTariffPrimeTimeWeekend) {
            MasterQuery.add(config.Models.tariff.modelName, { name: 'Prime time (выходные дни)', orderId: orderId });
        }
        let findTariffNews= await MasterQuery.findOne(config.Models.tariff.modelName, {
            query: { name: 'Новости', orderId: orderId }
        });
        if (!findTariffNews) {
            MasterQuery.add(config.Models.tariff.modelName, { name: 'Новости', orderId: orderId });
        }
    }
}
