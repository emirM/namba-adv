const jwt = require('jsonwebtoken');

module.exports = (app) => {
  app.use((req, res, next) => {
    let token = req.body.token || req.params.token || req.headers['nambaadv-access-token'];
    jwt.verify(token, app.get('superSecret'), (err, decoded) => {
      req.decoded = decoded;
      app.set('user', decoded);
      next();
    });
  });
};
