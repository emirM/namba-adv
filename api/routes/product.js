const express = require('express');
const router = express.Router();

// validate forms
const productForm = require('../forms/product');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new product. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.product.modelName, config.operationNames.isAdd),
    filters.input.validate(productForm), async (req, res) => {
        console.log('add new product');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.product.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit product. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.product.modelName, config.operationNames.isRead),
    filters.input.validate(productForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.product.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove product.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.product.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.product.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all product with categoryId. */
    router.get('/:categoryId', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.product.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            let categoryId = req.params.categoryId;
            if (!categoryId) {
                return res.status(200).json({
                    success: false,
                    msg: 'Параметры неправильно переданы'
                });
            }
            req.body.query = req.body.query || { categoryId: categoryId };
            req.body.populate = [{ path: 'calcId'}];
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.product.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all product. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.product.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.product.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/products', router);
};
