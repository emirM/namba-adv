const express = require('express');
const router = express.Router();

// validate forms
const orderShablonForm = require('../forms/order-shablon');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new orderShablon. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.orderShablon.modelName, config.operationNames.isAdd),
    filters.input.validate(orderShablonForm), async (req, res) => {
        console.log('add new order');
        req.body.userId = req.user.id;
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.orderShablon.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit order. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.orderShablon.modelName, config.operationNames.isRead),
    filters.input.validate(orderShablonForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.orderShablon.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove orderShablon.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.orderShablon.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.orderShablon.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all orderShablon. */
    router.get('/my', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.orderShablon.modelName, config.operationNames.isRead),
    async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            req.body.query = { userId: req.user.id };
            req.body.paginate = [{path: 'orders'}];
            console.log(req.body);
    
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.orderShablon.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all orderShablon. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.orderShablon.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.orderShablon.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/order-shablons', router);
};
