const express = require('express');
const router = express.Router();

// validate forms
const categoryProductForm = require('../forms/category-product');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new categoryProduct. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.categoryProduct.modelName, config.operationNames.isAdd),
    filters.input.validate(categoryProductForm), async (req, res) => {
        console.log('add new categoryProduct');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.categoryProduct.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit categoryProduct. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.categoryProduct.modelName, config.operationNames.isRead),
    filters.input.validate(categoryProductForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.categoryProduct.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove categoryProduct.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.categoryProduct.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.categoryProduct.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all categoryProduct. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.categoryProduct.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.categoryProduct.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/category-products', router);
};
