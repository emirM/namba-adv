const express = require('express');
const router = express.Router();

// validate forms
const orderForm = require('../forms/order');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new order. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.order.modelName, config.operationNames.isAdd),
    filters.input.validate(orderForm), async (req, res) => {
        console.log('add new order');
        try {
            req.body.userId = req.user.id;
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.order.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit order. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.order.modelName, config.operationNames.isRead),
    filters.input.validate(orderForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.order.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove order.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.order.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.order.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });


    /* GET all orders. */
    router.get('/my', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.order.modelName, config.operationNames.isRead),
    async (req, res) => {
        try {
            console.log('orders/my');
            req.body.query = req.body.query || {};
            req.body.query = { userId: req.user.id };
            console.log(req.body);
    
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.order.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all orders. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.order.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.order.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/orders', router);
};
