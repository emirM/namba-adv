const express = require('express');
const router = express.Router();

// validate forms
const clientForm = require('../forms/client');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new orderShablon. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.client.modelName, config.operationNames.isAdd),
    filters.input.validate(clientForm), async (req, res) => {
        console.log('add new client');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.client.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit client. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.client.modelName, config.operationNames.isRead),
    filters.input.validate(clientForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.client.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove client.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.client.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.client.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all clients. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.client.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.client.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/clients', router);
};
