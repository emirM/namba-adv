var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');

// services
const operationService = require('../services/operation');
const crypto = require('../services/crypto');

// validate forms
const userForm = require('../forms/user');

module.exports = (app, db) => {
  const MasterQuery = app.get('masterQuery');
  const config = app.get('config');
  const filters = app.get('filters');

  router.post('/login', filters.input.validate(userForm), async function (req, res) {
    try {
      console.log('/login');
      // Encrypt
      req.body.password = crypto.encrypt(req.body.password);
      let findUser = await MasterQuery.getOneBy(config.Models.user.modelName,
        { 
          query: { email: req.body.email, password: req.body.password }
        }
      );

      if (!findUser) {
        return res.status(200).json({
          success: false,
          msg: 'Пользователь с таким логином или паролем не найдено.'
        });
      }

      let operations = await MasterQuery.getAll(config.Models.operation.modelName, {
        query: { roleId: findUser.roleId }
      });
  
      let newModel = {};
      newModel.name = findUser.name;
      newModel.phone = findUser.phone;
      newModel.id = findUser.id;
      newModel.roleId = findUser.roleId;
      newModel.password = findUser.password;
      newModel.isAdmin = findUser.isAdmin || false;
      newModel.operations = operations.docs;

      let token = jwt.sign(newModel, app.get('superSecret'), {
        expiresIn: 86400 // expires in 24 hours
      });
      console.log(newModel);
      newModel.password = '';
      res.status(200).json({
        success: true,
        msg: 'ok',
        token: token,
        data: newModel
      });
    } catch(e) {
      console.log(e);
      return res.status(200).json({
        success: false,
        msg: 'Что то пошло не так!'
      });
    }
  });

  router.post('/register', filters.input.validate(userForm), async function (req, res) {
    try {
      req.body.password = crypto.encrypt(req.body.password);
      console.log(req.body);
      let findUser = await MasterQuery.getOneBy(config.Models.user.modelName, { query: { email: req.body.email } });
      if (findUser) {
        return res.status(200).json({
          success: false,
          msg: 'Такой email уже зарегистрирован'
        });
      }

      if (!req.body.roleId) {
        let role = await MasterQuery.getOneBy(config.Models.role.modelName, { query: {name: 'Гость'} });
        req.body.roleId = role ? role._id : '';
        if (!role) {
          role = await MasterQuery.add(config.Models.role.modelName, { name: 'Гость' });
          if (role && role._id) {
            operationService.addDefaultOperations(db, role._id);
            req.body.roleId = role._id;
          }
        }
      }
      
      let newModel = await MasterQuery.add(config.Models.user.modelName, req.body);
      if (!newModel) {
        return res.status(200).json({
          success: false,
          msg: 'ошибка при регистрации'
        });
      }

      let operations = await MasterQuery.getAll(config.Models.operation.modelName, {
        query: { roleId: newModel.id }
      });
 
      req.body.password = '';
      req.body.id = newModel.id;
      req.body.roleId = newModel.roleId;
      let token = jwt.sign(req.body, app.get('superSecret'), {
        expiresIn: 86400 // expires in 24 hours
      });

      newModel.password = '';
      newModel.operations = operations.docs;
      res.status(200).json({
        success: true,
        msg: 'ok',
        token: token,
        data: newModel
      });
    } catch(e) {
      console.log(e);
      return res.status(200).json({
        success: false,
        msg: 'Что то пошло не так!'
      });
    }
  });

  /* POST edit user. */
  router.post('/edit', filters.user.authRequired(),
    filters.user.checkRole(config.Models.user.modelName, config.operationNames.isEdit),
    filters.input.validate(userForm), async (req, res) => {
      console.log('edit');
      try {
          res.status(200).json({
              success: true,
              msg: 'ok',
              data: await MasterQuery.edit(config.Models.user.modelName, req.body)
          });
      } catch(e) {
          console.log(e);
          res.status(200).json({
              success: false,
              msg: 'something wrong!'
          });
      }
  });

  /* POST remove user. */
  router.post('/remove', filters.user.authRequired(),
    filters.user.checkRole(config.Models.user.modelName, config.operationNames.isRemove),
      async (req, res) => {
      console.log('user remove');
      try {
          res.status(200).json({
              success: true,
              msg: 'ok',
              data: await MasterQuery.remove(config.Models.user.modelName, req.body)
          });
      } catch(e) {
          console.log(e);
          res.status(200).json({
              success: false,
              msg: 'something wrong!'
          });
      }
  });

  router.get('/', async function(req, res, next) {
    let data = req.body || {};
    if (req.query) {
      if (req.query.text) {
        const regex = new RegExp('^' + req.query.text, 'i'); // ищет только начальное совпадение
        const regex2 = new RegExp('\\ ' + req.query.text, 'i'); // ищет слова с пробелСлова = \ слова/i
        data.query = {
            $or: [
              { name: regex },
              { name: regex2 }
            ]
        }
      }
      if (req.query.page && Number(req.query.page)) {
          data.page = req.query.page;
      }
      if (req.query.limit && Number(req.query.limit)) {
          data.limit = req.query.limit;
      }
    }

    res.status(200).json({
      success: true,
      msg: 'ok',
      data: await MasterQuery.getAll(config.Models.user.modelName, data)
    });
  });

  app.use('/users', router);
}
