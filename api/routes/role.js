const express = require('express');
const router = express.Router();

// services
const operationService = require('../services/operation');

// validate forms
const roleForm = require('../forms/role');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new currency. */
    router.post('/add', filters.user.authRequired(),
        filters.user.checkRole(config.Models.role.modelName, config.operationNames.isAdd),
        filters.input.validate(roleForm), async (req, res) => {
        console.log('add new role');
        try {
            const role = await MasterQuery.add(config.Models.role.modelName, req.body);
            if (role && role._id) {
                await operationService.addDefaultOperations(db, role._id);
            }
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: role
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit role. */
    router.post('/edit', filters.user.authRequired(),
        filters.user.checkRole(config.Models.role.modelName, config.operationNames.isEdit),
        filters.input.validate(roleForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.role.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove role. */
    router.post('/remove', filters.user.authRequired(),
        filters.user.checkRole(config.Models.role.modelName, config.operationNames.isRemove),
        async (req, res) => {
        console.log('role remove');
        try {
            const delRole = await MasterQuery.remove(config.Models.role.modelName, req.body);
            if (delRole && delRole._id) {
                await MasterQuery.removeAll(config.Models.operation.modelName, {query: {roleId: delRole._id}});
            }
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: delRole
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all operations by roleId. */
    router.get('/:id/operations', filters.user.authRequired(),
        filters.user.checkRole(config.Models.role.modelName, config.operationNames.isEdit),
        async (req, res) => {
        console.log('/:id/operations');
        try {
            req.body.query = { roleId: req.params.id };
            if (req.query && req.query.text) {
                const regex = new RegExp('^' + req.query.text, 'i'); // ищет только начальное совпадение
                const regex2 = new RegExp('\\ ' + req.query.text, 'i'); // ищет слова с пробелСлова = \ слова/i
                req.body.query.$or = [
                        { name: regex },
                        { name: regex2 }
                    ]
            }

            req.body.populate = req.body.populate || [];
            req.body.populate.push({ path: 'roleId', select: 'name _id' });
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.operation.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all roles in company. */
    router.get('/', filters.user.authRequired(),
        filters.user.checkRole(config.Models.role.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            if (req.query) {
                if (req.query.text) {
                    const regex = new RegExp('^' + req.query.text, 'i'); // ищет только начальное совпадение
                    const regex2 = new RegExp('\\ ' + req.query.text, 'i'); // ищет слова с пробелСлова = \ слова/i
                    req.body.query.$or = [
                        { name: regex },
                        { name: regex2 }
                    ]
                    console.log(req.body.query);
                }
                if (req.query.page && Number(req.query.page)) {
                    req.body.page = req.query.page;
                }
                if (req.query.limit && Number(req.query.limit)) {
                    req.body.limit = req.query.limit;
                }
            }
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.role.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/roles', router);
};
