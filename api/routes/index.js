var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');

// validate forms
const userForm = require('../forms/user');

module.exports = (app, db) => {
 const MasterQuery = app.get('masterQuery');
 const config = app.get('config');
 const filters = app.get('filters');


  router.get('/getByModelName/:modelName', async function(req, res, next) {
    try {
      let modelName = req.params.modelName;

      res.status(200).json({
        success: true,
        msg: 'ok',
        data: await MasterQuery.getAll(modelName, { limit: 100 })
      });
    } catch (e) {
      console.log(e);
      res.status(200).json({
        success: false,
        msg: 'ok'
      });
    }
  });

  router.get('/', function(req, res, next) {
    res.status(200).json({
      success: true,
      msg: 'ok'
    });
  });

  app.use('/', router);
}
