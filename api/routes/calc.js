const express = require('express');
const router = express.Router();

// validate forms
const calcForm = require('../forms/calc');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new calc. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.calc.modelName, config.operationNames.isAdd),
    filters.input.validate(calcForm), async (req, res) => {
        try {
            let name = req.body.name.replace(/[/]/g,'').replace(/[*]/g,'').replace(/[+]/g,'').replace(/[-]/g,'').replace(/[(]/g,'').replace(/[)]/g,'');
            const findCalc = await MasterQuery.getOneBy(config.Models.calc.modelName, { query: { name: name } });
            if (findCalc) {
                return res.status(200).json({
                    success: false,
                    msg: 'Калькулятор с таким именем уже имеется! Пожалуйста выберите другую'
                });
            }
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.calc.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit calc. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.calc.modelName, config.operationNames.isRead),
    filters.input.validate(calcForm), async (req, res) => {
        try {
            let name = req.body.name.replace(/[/]/g,'').replace(/[*]/g,'').replace(/[+]/g,'').replace(/[-]/g,'').replace(/[(]/g,'').replace(/[)]/g,'');
            const findCalc = await MasterQuery.getOneBy(config.Models.calc.modelName,
                { 
                    query: { name: name, _id: { $ne: req.body._id } }
                }
            );
            if (findCalc) {
                return res.status(200).json({
                    success: false,
                    msg: 'Калькулятор с таким именем уже имеется! Пожалуйста выберите другую'
                });
            }
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.calc.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove calc.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.calc.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.calc.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all calcs. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.calc.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            let modelKeys = Object.keys(config.Models);
            let models = [];
            for (let i=0; i<modelKeys.length; i++) {
                if (config.Models[modelKeys[i]].refCalc) {
                    models.push(config.Models[modelKeys[i]]);
                }
            }
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.calc.modelName, req.body),
                refModels: models
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/calcs', router);
};
