const express = require('express');
const router = express.Router();

// validate forms
const tvChannelForm = require('../forms/tv-channel');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');

    /* POST add new tvChannel. */
    router.post('/add', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.tvChannel.modelName, config.operationNames.isAdd),
    filters.input.validate(tvChannelForm), async (req, res) => {
        console.log('add new tvChannel', req.body);
        try {
            let tvChannel = await MasterQuery.add(config.Models.tvChannel.modelName, req.body);
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: tvChannel
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit tvChannel. */
    router.post('/edit', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.tvChannel.modelName, config.operationNames.isRead),
    filters.input.validate(tvChannelForm), async (req, res) => {
        console.log('edit');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.tvChannel.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove tvChannel.*/
    router.post('/remove', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.tvChannel.modelName, config.operationNames.isRemove), async (req, res) => {
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.tvChannel.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all TV channel. */
    router.get('/', filters.user.authRequired(), 
    filters.user.checkRole(config.Models.tvChannel.modelName, config.operationNames.isRead), async (req, res) => {
        try {
            req.body.query = req.body.query || {};
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.getAll(config.Models.tvChannel.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    app.use('/tv-channels', router);
};
