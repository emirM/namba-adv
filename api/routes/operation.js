const express = require('express');
const router = express.Router();

// services
const translitService = require('../services/translit');

// validate forms
const operationForm = require('../forms/operation');

module.exports = (app, db) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    const filters = app.get('filters');
    
    /* POST add new currency. */
    router.post('/add', filters.user.authRequired(), filters.input.validate(operationForm), async (req, res) => {
        console.log('add new operation');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.add(config.Models.operation.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST edit role. */
    router.post('/edit', filters.user.authRequired(), filters.input.validate(operationForm), async (req, res) => {
        console.log('edit', req.body);
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.edit(config.Models.operation.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* POST remove operation. */
    router.post('/remove', filters.user.authRequired(), async (req, res) => {
        console.log('operation remove');
        try {
            res.status(200).json({
                success: true,
                msg: 'ok',
                data: await MasterQuery.remove(config.Models.operation.modelName, req.body)
            });
        } catch(e) {
            console.log(e);
            res.status(200).json({
                success: false,
                msg: 'something wrong!'
            });
        }
    });

    /* GET all car. */
    router.get('/', filters.user.authRequired(), async (req, res) => {
        console.log(req.params);
    });

    app.use('/operations', router);
};
