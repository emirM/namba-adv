module.exports = {
    name: {
    },
    email: {
        length: {
            maximum: 125,
            message: '^Длина Почты должно быть меньше 125-символов.'
        }
    },
    password: {
        presence: {
            message: '^Пароль объязательное поле.'
        },
        length: {
            minimum: 5,
            message: '^Длина Наименование должно быть больше 5-символов.'
        }
    },
    _id: {
        
    },
    roleId: {
        
    }
};