module.exports = {
    name: {
        presence: {
            message: '^Наименование объязательное поле.'
        }
    },
    modelLabel: {
        presence: {
            message: '^Label объязательное поле.'
        }
    },
    roleId: {
        presence: {
            message: '^Выберите роль.'
        }
    },
    isRead: {
    },
    isAdd: {
    },
    isRemove: {
    },
    isEdit: {
    },
    _id: {
        
    }
};