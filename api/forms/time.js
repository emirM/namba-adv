module.exports = {
    name: {
        presence: {
            message: '^Наименование объязательное поле.'
        },
        length: {
            maximum: 125,
            message: '^Длина Наименование должно быть меньше 125-символов.'
        }
    },
    _id: {
        
    },
    price: {
        presence: {
            message: '^Цена объязательное поле.'
        },
        numericality: {
            message: '^Только цифры'
        }
    }
};
