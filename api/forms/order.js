module.exports = {
    calc: {
        presence: {
            message: '^Не проведена расчеты.'
        }
    },
    categoryId: {
        presence: {
            message: '^Выберите Категорию.'
        }
    },
    productId: {
        presence: {
            message: '^Выберите продукт.'
        }
    },
    clientId: {
    },
    _id: {
        
    },
    userId: {
        
    }
};