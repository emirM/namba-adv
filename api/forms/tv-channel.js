module.exports = {
    name: {
        presence: {
            message: '^Наименование объязательное поле.'
        },
        length: {
            maximum: 125,
            message: '^Длина Наименование должно быть меньше 125-символов.'
        }
    },
    _id: {
        
    },
    tariffs: {
        presence: {
            message: '^Тариф объязательное поле.'
        }
    },
    coefficients: {
        presence: {
            message: '^Коэфициент объязательное поле.'
        }
    }
};
