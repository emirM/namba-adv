module.exports = {
    name: {
        presence: {
            message: '^Наименование объязательное поле.'
        },
        length: {
            maximum: 125,
            message: '^Длина Наименование должно быть меньше 125-символов.'
        }
    },
    categoryId: {
        presence: {
            message: '^Выберите к категорию.'
        }
    },
    calcId: {
        presence: {
            message: '^Выберите калькулятор для данного продукта.'
        }
    },
    _id: {
        
    }
};
