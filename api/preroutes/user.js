module.exports = (filters, app) => {
    const MasterQuery = app.get('masterQuery');
    const config = app.get('config');
    // ---------------------------------------------------------
    // route middleware to authenticate and check token
    // ---------------------------------------------------------

    filters['user'] = filters['user'] || {};

    filters.user.authRequired = () => {
        return (req, res, next) => {
            //console.log(req.decoded);
            if (!req.decoded) {
                return res.status(200).json({
                    success: false,
                    msg: 'Вы не авторизованы',
                    status: 'yellow',
                    data: {
                        code: 403,
                        message: 'Not authorize',
                        isNotAuth: true
                    }
                });
            }
            req.user = req.decoded;
            next();
        }
    }

    filters.user.isAdmin = () => {
        const user = req.decoded._doc;
        return (req, res, next) => {
            if (!user|| user.password !== config.adminPassword) {
                return res.status(200).json({
                    success: false,
                    msg: 'У вас не достаточно прав',
                    status: 'yellow',
                    data: {
                        code: 403,
                        isNotAdmin: true
                    }
                });
            }
            req.user = req.decoded._doc;
            next();
        }
    }

    filters.user.checkRole = (modelName, operationName) => {
        return async (req, res, next) => {
            const user = req.user;
            console.log('checkRole', user);
            if (user.isAdmin) {
                console.log(user.name + ' Админ - Доступ открыт!');
                return next();
            }
            if (!modelName || !operationName) {
                console.log('modelName is null or operationName');
                return res.status(200).json({
                    success: false,
                    msg: 'Доступ запрещен!'
                });
            }
            let role = await MasterQuery.getOneBy(config.Models.role.modelName, {query: {_id: user.roleId}});
            if (!role) {
                console.log('role is null');
                return res.status(200).json({
                    success: false,
                    msg: 'Доступ запрещен! Не установлена роль.'
                });
            }
            console.log('role', role);
            let operation = await MasterQuery.getOneBy(config.Models.operation.modelName, {
                query: { modelLabel: modelName, roleId: role._id }
            });
            console.log({ modelLabel: modelName, roleId: role._id });
            if (!operation) {
                console.log('operation is null');
                return res.status(200).json({
                    success: false,
                    msg: 'Доступ запрещен! Не определенны операции для этой роли.'
                });
            }

            if (!operation[operationName]) {
                return res.status(200).json({
                    success: false,
                    msg: 'Доступ запрещен! У ваc нет прав.'
                });
            }
            
            next();
        }
    }
}