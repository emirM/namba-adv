// core
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// rxjs
import { Observable, of } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

// config
import { Methods } from '../../config';

// services
import { HandleErrorService } from '../handle-error/handle-error.service';
import { MyLocalStorageService } from '../local-storage/local-storage.service';


@Injectable({
  providedIn: 'root'
})
export class MasterQueryService {

  constructor(
    private http: HttpClient,
    private handleService: HandleErrorService,
    private localStorage: MyLocalStorageService
  ) { }


  query(url: string, method: string, data: any, auth: boolean) {
    if (!method || !data || !url) {
      return this.handleService.returnError('dates not valid');
    }

    switch(method) {
      case Methods.post:
        return this.post(data, url, auth);
      case Methods.put:
        return this.put(data, url, auth);
      case Methods.get:
        return this.get(url, auth);
      case Methods.delete:
        return this.delete(url, auth);
    }
  }

  post(url: string, data: any, auth: boolean): Observable<any> {
    const token = this.localStorage.getToken() || '';
    if (!token && auth) {
      return this.handleService.returnError('token is null');
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'nambaadv-access-token': token.toString()
      })
    };
    return this.http.post(url, data, httpOptions)
    .pipe(
      catchError(this.handleService.handleErrorNew)
    );
  }

  put(url: string, data: any, auth: boolean): Observable<any> {
    const token = this.localStorage.getToken() || '';
    if (!token && auth) {
      return this.handleService.returnError('token is null');
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'nambaadv-access-token': token.toString()
      })
    };
    return this.http.post(url, data, httpOptions)
    .pipe(
      catchError(this.handleService.handleErrorNew)
    );
  }

  get(url: string, auth: boolean): Observable<any> {
    const token = this.localStorage.getToken() || '';
    if (!token && auth) {
      return this.handleService.returnError('token is null');
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'nambaadv-access-token': token.toString()
      })
    };
    return this.http.get(url, httpOptions)
    .pipe(
      catchError(this.handleService.handleErrorNew)
    );
  }

  delete(url: string, auth: boolean): Observable<any> {
    const token = this.localStorage.getToken() || '';
    if (!token && auth) {
      return this.handleService.returnError('token is null');
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'nambaadv-access-token': token.toString()
      })
    };

    return this.http.delete(url, httpOptions)
    .pipe(
      catchError(this.handleService.handleErrorNew)
    );
  }
}
