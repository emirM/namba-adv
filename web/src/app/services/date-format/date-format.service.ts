import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateFormatService {

  constructor() { }
  doNormalFormatDate(dateNumber: any): string {
    if (!dateNumber) {
      return '';
    }
    const date = new Date(dateNumber);
    const nowDate = new Date();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const month = date.getMonth() + 1;
    const days = date.getDate();

    let normalDate: string = (days > 9 ? days : '0' + days) + '.' + (month > 9 ? month : '0' + month) + '.' + date.getFullYear() + ' ' +
    (hours > 9 ? hours : '0' + hours) + ':' + (minutes > 9 ? minutes : '0' + minutes);
    return normalDate;
  }
}
