// core
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import * as firebase from 'firebase';

// config
import {
  ApiConfig, NotifyConfig
} from '../../config';

// models
import { User } from '../../models/user';

// services
import { MyLocalStorageService } from '../local-storage/local-storage.service';

declare let $: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser;

  constructor(
    private localStorage: MyLocalStorageService,
    private router: Router
  ) {
    this.currentUser = firebase.auth().currentUser;
  }

  logout() {
    this.localStorage.clearStorage();
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
        console.log('Sign-out successful');
    }).catch(function(error) {
        // An error happened.
        console.log(error);
    });
    return this.router.navigate(['/account/login']);
  }

  isCheckAuthRedirectToLogin(): Boolean { // проверка что пользователь авторизован
    console.log('-----------проверка что пользователь авторизован-------------------');
    const user = this.localStorage.getUser();
    console.log(user);
    if (!user) {
      console.log('Вы не авторизованы');
      this.logout();
      return true;
    }
    return false;
  }

  isCheckAuthRedirectToProfile(): Boolean { // проверка что пользователь авторизован
    console.log('-----------isCheckAuthRedirectToProfile----------');
    const user = this.localStorage.getUser();
    if (user) { // if (user && user._id && token) {
      console.log('Вы авторизованы');
      this.router.navigate(['/order']);
      return true; 
    }
    return false;
  }

  checkAuth() {
    console.log('checkAuth');
    const user = this.localStorage.getUser();
    if (!user) {
      this.logout();
    }
  }
}
