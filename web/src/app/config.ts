import { environment } from '../environments/environment';
const apiUrl = environment.apiUrl;
export const PageLimit = 30;

export const Methods = {
  post: 'POST',
  put: 'PUT',
  delete: 'DELETE',
  get: 'GET'
}

export const Times = [
  {
    option: '600',
    weekendOption: 'w600123',
    startTime: '6:00',
    endTime: '6:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '600',
    weekendOption: 'w600',
    startTime: '6:00',
    endTime: '6:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '630',
    weekendOption: 'w630',
    startTime: '6:30',
    endTime: '7:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '700',
    weekendOption: 'w700',
    startTime: '7:00',
    endTime: '7:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '730',
    weekendOption: 'w730',
    startTime: '7:30',
    endTime: '8:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '800',
    weekendOption: 'w800',
    startTime: '8:00',
    endTime: '8:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '830',
    weekendOption: 'w830',
    startTime: '8:30',
    endTime: '9:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '900',
    weekendOption: 'w900',
    startTime: '9:00',
    endTime: '9:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '930',
    weekendOption: 'w930',
    startTime: '9:30',
    endTime: '10:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1000',
    weekendOption: 'w1000',
    startTime: '10:00',
    endTime: '10:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1030',
    weekendOption: 'w1030',
    startTime: '10:30',
    endTime: '11:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1100',
    weekendOption: 'w1100',
    startTime: '11:00',
    endTime: '11:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1130',
    weekendOption: 'w1130',
    startTime: '11:30',
    endTime: '12:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1200',
    weekendOption: 'w1200',
    startTime: '12:00',
    endTime: '12:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1230',
    weekendOption: 'w1230',
    startTime: '12:30',
    endTime: '13:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1300',
    weekendOption: 'w1300',
    startTime: '13:00',
    endTime: '13:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1330',
    weekendOption: 'w1330',
    startTime: '13:30',
    endTime: '14:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1400',
    weekendOption: 'w1400',
    startTime: '14:00',
    endTime: '14:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1430',
    weekendOption: 'w1430',
    startTime: '14:30',
    endTime: '15:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1500',
    weekendOption: 'w1500',
    startTime: '15:00',
    endTime: '15:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1530',
    weekendOption: 'w1530',
    startTime: '15:30',
    endTime: '16:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1600',
    weekendOption: 'w1600',
    startTime: '16:00',
    endTime: '16:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1630',
    weekendOption: 'w1630',
    startTime: '16:30',
    endTime: '17:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1700',
    weekendOption: 'w1700',
    startTime: '17:00',
    endTime: '17:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1730',
    weekendOption: 'w1730',
    startTime: '17:30',
    endTime: '18:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1800',
    weekendOption: 'w1800',
    startTime: '18:00',
    endTime: '18:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1830',
    weekendOption: 'w1830',
    startTime: '18:30',
    endTime: '19:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1900',
    weekendOption: 'w1900',
    startTime: '19:00',
    endTime: '19:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '1930',
    weekendOption: 'w1930',
    startTime: '19:30',
    endTime: '20:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2000',
    weekendOption: 'w2000',
    startTime: '20:00',
    endTime: '20:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2030',
    weekendOption: 'w2030',
    startTime: '20:30',
    endTime: '21:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2100',
    weekendOption: 'w2100',
    startTime: '21:00',
    endTime: '21:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2130',
    weekendOption: 'w2130',
    startTime: '21:30',
    endTime: '22:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2200',
    weekendOption: 'w2200',
    startTime: '22:00',
    endTime: '22:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2230',
    weekendOption: 'w2230',
    startTime: '22:30',
    endTime: '23:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2300',
    weekendOption: 'w2300',
    startTime: '23:00',
    endTime: '23:30',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  },
  {
    option: '2330',
    weekendOption: 'w2330',
    startTime: '23:30',
    endTime: '24:00',
    value: 0,
    mondaySelected: false,
    tuesdaySelected: false,
    wednesdaySelected: false,
    thursdaySelected: false,
    fridaySelected: false,
    saturdaySelected: false,
    sundaySelected: false
  }
];

export const RadioCoefficients = [
  {
    name: 'Охват Кыргызстан',
    option: 'coverageKyrgyzstan123',
    weekendOption: 'wCoverageKyrgyzstan',
    value: 0,
    checked: false
  },
  {
    name: 'Охват Кыргызстан',
    option: 'coverageKyrgyzstan',
    weekendOption: 'wCoverageKyrgyzstan',
    value: 0,
    checked: false
  },
  {
    name: 'Охват Кыргызстан Выходные',
    option: 'coverageKyrgyzstanWeekend',
    weekendOption: 'wCoverageKyrgyzstanWeekend',
    value: 0,
    checked: false
  },
  {
    name: 'Первый в блоке',
    option: 'firstBlock',
    weekendOption: 'wFirstBlock',
    value: 0,
    checked: false
  },
  {
    name: 'Последний в блоке',
    option: 'lastBlock',
    weekendOption: 'wLastBlock',
    value: 0,
    checked: false
  },
  {
    name: 'Более 1 бренда в ролике',
    option: 'moreBrends',
    weekendOption: 'wMoreBrends',
    value: 0,
    checked: false
  },
  {
    name: 'Отсутствие конкурента в смежной позиции',
    option: 'withoutAppanents',
    weekendOption: 'wWithoutAppanents',
    value: 0,
    checked: false
  },
  {
    name: 'Длительность до 10 сек. Включительно',
    option: 'duration',
    weekendOption: 'wDuration',
    value: 0,
    checked: false
  },
  {
    name: 'Политическая реклама',
    option: 'political',
    weekendOption: 'wPolitical',
    value: 0,
    checked: false
  },
  {
    name: 'Наценка в декабре',
    option: 'extraChargeInDecember',
    weekendOption: 'wExtraChargeInDecember',
    value: 0,
    checked: false
  }
];

export const Models = {
  role: {
    modelName: 'Role',
    collectionName: 'roles',
    name: 'Роли',
    refCalc: true
  },
  radio: {
    modelName: 'Radio',
    collectionName: 'radios',
    name: 'Радио',
    refCalc: true
  },
  operation: {
    modelName: 'Operation',
    collectionName: 'operations',
    name: 'Операции',
    refCalc: true
  },
  user: {
    modelName: 'User',
    collectionName: 'users',
    name: 'Пользователи',
    refCalc: true
  },
  userInRole: {
    modelName: 'UserInRole',
    collectionName: 'userInRole',
    name: 'Пользователи в ролях',
    refCalc: true
  },
  categoryProduct: {
    modelName: 'CategoryProduct',
    collectionName: 'categoryProducts',
    name: 'Категория',
    refCalc: true
  },
  product: {
    modelName: 'Product',
    collectionName: 'products',
    name: 'Продукт',
    refCalc: true
  },
  tvChannel: {
    modelName: 'TVChannel',
    collectionName: 'tvchannels',
    name: 'Телеканал',
    refCalc: true
  },
  calc: {
    modelName: 'Calc',
    name: 'Калькулятор',
    collectionName: 'calcs',
    refCalc: true
  },
  order: {
    modelName: 'Order',
    collectionName: 'orders',
    name: 'Заказ',
    refCalc: true
  },
  orderShablon: {
    modelName: 'OrderShablon',
    collectionName: 'orderShablons',
    name: 'Шаблоны'
  },
  client: {
    modelName: 'Client',
    collectionName: 'clients',
    name: 'Клиент'
  },
  taxiPriceList: {
    modelName: 'TaxiPriceList',
    collectionName: 'taxiPriceLists',
    name: 'Прайс лист такси'
  },
  taxiProduct: {
    modelName: 'TaxiProduct',
    collectionName: 'taxiProducts',
    name: 'Клиент'
  }
}

export const ApiConfig = {
    root_url: apiUrl,
    tokenName: 'nambaadv-access-token',
    auth: {
      login: {
        url: apiUrl + '/users/login',
        method: Methods.post,
        fields: 'username,password'
      }
    },
    user: {
      register: {
        url: apiUrl + '/users/register', //{username, password, first_name, phone, email}
        method: Methods.post,
        fields: 'User'
      },
      edit: {
        url: apiUrl + '/users/edit',
        method: Methods.post,
        fields: 'User'
      },
      remove: {
        url: apiUrl + '/users/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/users',
        method: Methods.get,
        fields: ''
      }
    },
    tariff: {
      add: {
        url: apiUrl + '/tariffs/add',
        method: Methods.post,
        fields: 'Tariff'
      },
      edit: {
        url: apiUrl + '/tariffs/edit',
        method: Methods.post,
        fields: 'Tariff'
      },
      remove: {
        url: apiUrl + '/tariffs/remove',
        method: Methods.post,
        fields: '_id'
      },
      getQuery: {
        url: apiUrl + '/tariffs/getQuery',
        method: Methods.post,
        fields: 'query'
      },
      getAll: {
        url: apiUrl + '/tariffs',
        methos: Methods.get
      },
    },
    categoryProduct: {
      add: {
        url: apiUrl + '/category-products/add',
        method: Methods.post,
        fields: 'CategoryProduct'
      },
      edit: {
        url: apiUrl + '/category-products/edit',
        method: Methods.post,
        fields: 'CategoryProduct'
      },
      remove: {
        url: apiUrl + '/category-products/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/category-products',
        methos: Methods.get
      },
    },
    product: {
      add: {
        url: apiUrl + '/products/add',
        method: Methods.post,
        fields: 'Product'
      },
      edit: {
        url: apiUrl + '/products/edit',
        method: Methods.post,
        fields: 'Product'
      },
      remove: {
        url: apiUrl + '/products/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/products',
        methos: Methods.get
      },
      getByCategoryId: {
        url: apiUrl + '/products/:categoryId',
        methos: Methods.get
      }
    },
    route: {
      add: {
        url: apiUrl + '/routes/add',
        method: Methods.post,
        fields: 'RouteCity'
      },
      edit: {
        url: apiUrl + '/routes/edit',
        method: Methods.post,
        fields: 'RouteCity'
      },
      remove: {
        url: apiUrl + '/routes/remove',
        method: Methods.post,
        fields: '_id'
      },
      getQuery: {
        url: apiUrl + '/routes/getQuery',
        method: Methods.post,
        body: {} ////query = можно для монго query отправлят
      },
      getAll: {
        url: apiUrl + '/routes',
        methos: Methods.get
      },
    },
    role: {
      add: {
        url: apiUrl + '/roles/add',
        method: Methods.post,
        fields: 'Role'
      },
      edit: {
        url: apiUrl + '/roles/edit',
        method: Methods.post,
        fields: 'Role'
      },
      remove: {
        url: apiUrl + '/roles/remove',
        method: Methods.post,
        fields: '_id'
      },
      getOperations: {
        url: apiUrl + '/roles/:id/operations',
        method: Methods.get,
        fields: 'Operation'
      },
      getAll: {
        url: apiUrl + '/roles',
        methos: Methods.get
      }
    },
    operation: {
      add: {
        url: apiUrl + '/operations/add',
        method: Methods.post,
        fields: 'Operation'
      },
      edit: {
        url: apiUrl + '/operations/edit',
        method: Methods.post,
        fields: 'Operation'
      },
      remove: {
        url: apiUrl + '/operations/remove',
        method: Methods.post,
        fields: '_id'
      }
    },
    tvChannel: {
      add: {
        url: apiUrl + '/tv-channels/add',
        method: Methods.post,
        fields: 'TVChannel'
      },
      edit: {
        url: apiUrl + '/tv-channels/edit',
        method: Methods.post,
        fields: 'TVChannel'
      },
      remove: {
        url: apiUrl + '/tv-channels/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/tv-channels',
        methos: Methods.get
      },
    },
    time: {
      add: {
        url: apiUrl + '/times/add',
        method: Methods.post,
        fields: 'Time'
      },
      edit: {
        url: apiUrl + '/times/edit',
        method: Methods.post,
        fields: 'Time'
      },
      remove: {
        url: apiUrl + '/times/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/times',
        methos: Methods.get
      },
    },
    calc: {
      add: {
        url: apiUrl + '/calcs/add',
        method: Methods.post,
        fields: 'Calc'
      },
      edit: {
        url: apiUrl + '/calcs/edit',
        method: Methods.post,
        fields: 'Calc'
      },
      remove: {
        url: apiUrl + '/calcs/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/calcs',
        methos: Methods.get
      }
    },
    model: {
      getByModelName: {
        url: apiUrl + '/getByModelName/:modelName',
        method: Methods.get,
        fields: 'query: modelName'
      }
    },
    order: {
      add: {
        url: apiUrl + '/orders/add',
        method: Methods.post,
        fields: 'Order'
      },
      edit: {
        url: apiUrl + '/orders/edit',
        method: Methods.post,
        fields: 'Order'
      },
      remove: {
        url: apiUrl + '/orders/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/orders',
        methos: Methods.get
      },
      my: {
        url: apiUrl + '/orders/my',
        methos: Methods.get
      }
    },
    orderShablon: {
      add: {
        url: apiUrl + '/order-shablons/add',
        method: Methods.post,
        fields: 'OrderShablon'
      },
      edit: {
        url: apiUrl + '/order-shablons/edit',
        method: Methods.post,
        fields: 'OrderShablon'
      },
      remove: {
        url: apiUrl + '/order-shablons/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/order-shablons',
        methos: Methods.get
      },
      my: {
        url: apiUrl + '/order-shablons/my',
        methos: Methods.get
      }
    },
    client: {
      add: {
        url: apiUrl + '/clients/add',
        method: Methods.post,
        fields: 'Client'
      },
      edit: {
        url: apiUrl + '/clients/edit',
        method: Methods.post,
        fields: 'Client'
      },
      remove: {
        url: apiUrl + '/clients/remove',
        method: Methods.post,
        fields: '_id'
      },
      getAll: {
        url: apiUrl + '/clients',
        methos: Methods.get
      }
    }
}

export const NotifyConfig = {
  msgTypes: {
    primary: {
      styleName: 'bootstrap-primary',
      value: 'primary'
    },
    secondary: {
      styleName: 'bootstrap-secondary',
      value: 'secondary'
    },
    success: {
      styleName: 'bootstrap-success',
      value: 'success'
    },
    danger: {
      styleName: 'bootstrap-danger',
      value: 'danger'
    },
    warning: {
      styleName: 'bootstrap-warning',
      value: 'warning'
    },
    info: {
      styleName: 'bootstrap-info',
      value: 'info'
    },
    light: {
      styleName: 'bootstrap-light',
      value: 'light'
    },
    dark: {
      styleName: 'bootstrap-dark',
      value: 'dark'
    }
  }
};
