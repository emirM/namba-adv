// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';

// config
import { ApiConfig, NotifyConfig, Models } from '../../config';

// model
import { User } from '../../models/user';
import { ResponseApi } from '../../models/response-api';
import { UserInRole } from '../../models/user-in-role';
import { Role } from '../../models/role';

//services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { AuthService } from '../../services/auth/auth.service';
import { NotifyService } from '../../services/notify/notify.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';

declare let $: any;

@Component({
  selector: 'app-user-template',
  templateUrl: './user-template.component.html',
  styleUrls: ['./user-template.component.css']
})
export class UserTemplateComponent implements OnInit, OnDestroy {
  subscribes = new Array<any>();
  userInRoles = new Array<any>();
  newUser = new User();
  removeUser = new User();
  users = Array<User>()
  roles = new Array<any>();
  db = firebase.firestore();

  constructor(
    private masterQueryService: MasterQueryService,
    private authService: AuthService,
    private notifyService: NotifyService,
    private unsubscribeService: UnsubscribeService
  ) { }

  ngOnInit() {
    this.getAll();
    this.getRoles();
  }

  getAll() {
    return this.db.collection(Models.userInRole.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newUserInRoles = doc.data();
        newUserInRoles.uid = doc.id;
        this.userInRoles.push(newUserInRoles);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getRoles() {
    return this.db.collection(Models.role.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newRole = doc.data();
          newRole.uid = doc.id;
          this.roles.push(newRole);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  add() {
    /*this.authService.checkAuth();
    let newProduct = JSON.stringify(this.newUser);
    return this.db.collection(Models.product.collectionName).add(JSON.parse(newProduct)).then(
      (res) => {
        console.log(res);
        let newAddProduct = this.newProduct;
        newAddProduct.uid = res.id;
        this.products.push(newAddProduct);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newProduct = new Product();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });*/
    this.subscribes.push(
      this.masterQueryService.post(ApiConfig.user.register.url, this.newUser, true).subscribe(
        (res: ResponseApi) => {
          this.authService.checkAuth();
          if (!res.success) {
            return this.notifyService.showMessage(res.msg || 'Что то пошло не так.', NotifyConfig.msgTypes.warning.styleName);
          }
          let newAddUser: User = res.data;
          this.users.push(newAddUser);
          this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
          this.newUser = new User();
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage('Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
        }
      )
    );
  }

  edit(user: User) {
    if (!user) {
      console.log('car is null');
      return;
    }
    this.subscribes.push(
      this.masterQueryService.post(ApiConfig.user.edit.url, user, true).subscribe(
        (res: ResponseApi) => {
          this.authService.checkAuth();
          if (!res.success) {
            return this.notifyService.showMessage(res.msg || 'Что то пошло не так.', NotifyConfig.msgTypes.warning.styleName);
          }
          this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage('Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
        }
      )
    );
  }
  
  remove(userId: string) {
    this.subscribes.push(
      this.masterQueryService.post(ApiConfig.user.remove.url, { uid: userId }, true).subscribe(
        (res: ResponseApi) => {
          this.authService.checkAuth();
          if (!res.success) {
            return this.notifyService.showMessage(res.msg || 'Что то пошло не так.', NotifyConfig.msgTypes.warning.styleName);
          }
          this.users = this.users.filter(x => x.uid !== userId);
          this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
          this.removeUser = new User();
          $('#remove-modal').modal('toggle');
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage('Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
        }
      )
    );
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
