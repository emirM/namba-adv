// core
import { Component, OnInit } from '@angular/core';

// config
import { Models } from '../../config';

// models
import { User } from '../../models/user';
import { Operation } from '../../models/operation';

// services
import { AuthService } from '../../services/auth/auth.service';
import { MyLocalStorageService } from '../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-navbar-header',
  templateUrl: './navbar-header.component.html',
  styleUrls: ['./navbar-header.component.css']
})
export class NavbarHeaderComponent implements OnInit {
  currentUser = new User();
  operation = {
    categoryOperation: new Operation(),
    productOperation: new Operation(),
    tvChannelOperation: new Operation(),
    calcOperation: new Operation(),
    roleOperation: new Operation(),
    userOperation: new Operation()
  };

  constructor(
    private authService: AuthService,
    private localStorageService: MyLocalStorageService
  ) {
  }

  ngOnInit() {
    this.currentUser = this.localStorageService.getUser();
    this.initOperations();
  }

  initOperations() {
    console.log('initOperations', this.currentUser);
    if (!this.currentUser || !Array.isArray(this.currentUser.operations)) {
      return console.log('operations is null');
    }

    let categoryOperationFind = this.currentUser.operations.filter(x => x.modelLabel === Models.categoryProduct.modelName)[0];
    if (!categoryOperationFind) return console.log('not found operation');
    this.operation.categoryOperation = categoryOperationFind;

    let productOperationFind = this.currentUser.operations.filter(x => x.modelLabel === Models.product.modelName)[0];
    if (!productOperationFind) return console.log('not found operation');
    this.operation.productOperation = productOperationFind;

    let tvChannelOperationFind = this.currentUser.operations.filter(x => x.modelLabel === Models.tvChannel.modelName)[0];
    if (!tvChannelOperationFind) return console.log('not found operation');
    this.operation.tvChannelOperation = tvChannelOperationFind;

    let calcOperationFind = this.currentUser.operations.filter(x => x.modelLabel === Models.calc.modelName)[0];
    if (!calcOperationFind) return console.log('not found operation');
    this.operation.calcOperation = calcOperationFind;

    let roleOperationFind = this.currentUser.operations.filter(x => x.modelLabel === Models.role.modelName)[0];
    if (!roleOperationFind) return console.log('not found operation');
    this.operation.roleOperation = roleOperationFind;

    let userOperationFind = this.currentUser.operations.filter(x => x.modelLabel === Models.user.modelName)[0];
    if (!userOperationFind) return console.log('not found operation');
    this.operation.userOperation = userOperationFind;

    console.log(this.operation);
  }

  doLogout() {
    return this.authService.logout();
  }

  calc() {
    
  }
}
