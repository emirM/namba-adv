import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvChannelTemplateComponent } from './tv-channel-template.component';

describe('TvChannelTemplateComponent', () => {
  let component: TvChannelTemplateComponent;
  let fixture: ComponentFixture<TvChannelTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvChannelTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvChannelTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
