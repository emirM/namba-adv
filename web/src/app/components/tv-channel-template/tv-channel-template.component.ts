// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';

// config
import { NotifyConfig, Models } from '../../config';

// models
import { TVChannel } from '../../models/tv-channel';
import { Coefficient } from '../../models/coefficient';
import { Tariff } from '../../models/tariff';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { AuthService } from '../../services/auth/auth.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';

// constants
declare let $: any;

@Component({
  selector: 'app-tv-channel-template',
  templateUrl: './tv-channel-template.component.html',
  styleUrls: ['./tv-channel-template.component.css']
})
export class TvChannelTemplateComponent implements OnInit, OnDestroy {
  newTVChannel= new TVChannel();
  tvChannels = new Array<any>();
  removeChannel = new TVChannel();
  subscribes = new Array<any>();
  coefficients = new Array<Coefficient>();
  tariffs = new Array<Tariff>();
  products = new Array<any>();
  db = firebase.firestore();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private authService: AuthService,
    private unsubscribeService: UnsubscribeService
  ) { }

  ngOnInit() {
    this.getAll();
    this.initCoefficients();
    this.initTarrifs();
    this.getAllProducts();
  }


  getAllProducts() {
    this.authService.checkAuth();
    return this.db.collection(Models.product.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          let newProduct = doc.data();
          newProduct.uid = doc.id;
          this.products.push(newProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  initCoefficients() {
    this.coefficients = [];
    let newFirstBlock = new Coefficient();
    newFirstBlock.value = 0;
    newFirstBlock.name = 'Первый в блоке';
    this.coefficients.push(newFirstBlock);

    let newLastBlock = new Coefficient();
    newLastBlock.value = 0;
    newLastBlock.name = 'Последний в блоке';
    this.coefficients.push(newLastBlock);

    let newMoreBrands = new Coefficient();
    newMoreBrands.value = 0;
    newMoreBrands.name = 'Присутствие двух и более брендов';
    this.coefficients.push(newMoreBrands);

    let newWithoutPriority = new Coefficient();
    newWithoutPriority.value = 0;
    newWithoutPriority.name = 'Без приоритета';
    this.coefficients.push(newWithoutPriority);
  }

  initTarrifs() {
    this.tariffs = [];
    let newOfftimeWeekdays = new Tariff();
    newOfftimeWeekdays.price = 0;
    newOfftimeWeekdays.name = 'Off time (будние дни)';
    this.tariffs.push(newOfftimeWeekdays);

    let newOfftimeWeekend = new Tariff();
    newOfftimeWeekend.price = 0;
    newOfftimeWeekend.name = 'Off time (выходные дни)';
    this.tariffs.push(newOfftimeWeekend);

    let newPrePrimeTime = new Tariff();
    newPrePrimeTime.price = 0;
    newPrePrimeTime.name = 'Pre prime time';
    this.tariffs.push(newPrePrimeTime);

    let newPrimeTimeWeekdays = new Tariff();
    newPrimeTimeWeekdays.price = 0;
    newPrimeTimeWeekdays.name = 'Prime time (будние дни)';
    this.tariffs.push(newPrimeTimeWeekdays);

    let newPrimeTimeWeekend = new Tariff();
    newPrimeTimeWeekend.price = 0;
    newPrimeTimeWeekend.name = 'Prime time (выходные дни)';
    this.tariffs.push(newPrimeTimeWeekend);

    let newNews= new Tariff();
    newNews.price = 0;
    newNews.name = 'Новости';
    this.tariffs.push(newNews);

    let newFixPrice= new Tariff();
    newFixPrice.price = 0;
    newFixPrice.name = 'Фиксированная цена';
    this.tariffs.push(newFixPrice);
  }

  getAll() {
    console.log('getAll');
    return this.db.collection(Models.tvChannel.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newTvChannel = doc.data();
          newTvChannel.uid = doc.id;
          this.tvChannels.push(newTvChannel);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  add() {
    this.newTVChannel.coefficients = this.coefficients;
    this.newTVChannel.tariffs = this.tariffs;

    let newTVChannel= JSON.stringify(this.newTVChannel);
    return this.db.collection(Models.tvChannel.collectionName).add(JSON.parse(newTVChannel)).then(
      (res) => {
        console.log(res);
        this.newTVChannel.uid = res.id;
        this.tvChannels.unshift(this.newTVChannel);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newTVChannel = new TVChannel();
        this.initCoefficients();
        this.initTarrifs();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  edit(tvChannel: TVChannel) {
    if (!tvChannel) {
      console.log('tvChannel is null');
      return;
    }

    this.authService.checkAuth();
    this.db.doc(Models.tvChannel.collectionName + '/' + tvChannel.uid).update(tvChannel).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }
  
  remove(tvChannelId: string) {
    if (!tvChannelId) {
      console.log('tvChannelId is null');
      return;
    }

    this.db.doc(Models.tvChannel.collectionName + '/' + tvChannelId).delete().then(
      (res) => {
        console.log(res);
        this.tvChannels = this.tvChannels.filter(x => x.uid !== tvChannelId);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.removeChannel = new TVChannel();
        $('#remove-modal').modal('toggle');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
