// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';

// config
import { ApiConfig, NotifyConfig, Models } from '../../config';

// models
import { Product } from '../../models/product';
import { CategoryProduct } from '../../models/category-product';
import { ResponseApi } from '../../models/response-api';
import { Calc } from '../../models/calc';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { AuthService } from '../../services/auth/auth.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';

// constants
declare let $: any;

@Component({
  selector: 'app-product-template',
  templateUrl: './product-template.component.html',
  styleUrls: ['./product-template.component.css']
})
export class ProductTemplateComponent implements OnInit, OnDestroy {
  newProduct= new Product();
  products = new Array<any>();
  categoryProducts = new Array<any>();
  removeProduct = new Product();
  subscribes = new Array<any>();
  calcs = new Array<any>();
  db = firebase.firestore();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private authService: AuthService,
    private unsubscribeService: UnsubscribeService
  ) { }

  ngOnInit() {
    this.getAll();
    this.getAllCategoryProducts();
    this.getCalcs();
  }

  getAll() {
    this.authService.checkAuth();
    return this.db.collection(Models.product.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newProduct = doc.data();
          newProduct.uid = doc.id;
          this.products.push(newProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getCalcs() {
    this.authService.checkAuth();
    return this.db.collection(Models.calc.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newCalc = doc.data();
          newCalc.uid = doc.id;
          this.calcs.push(newCalc);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  add() {
    this.authService.checkAuth();
    let newProduct = JSON.stringify(this.newProduct);
    return this.db.collection(Models.product.collectionName).add(JSON.parse(newProduct)).then(
      (res) => {
        console.log(res);
        let newAddProduct = this.newProduct;
        newAddProduct.uid = res.id;
        this.products.push(newAddProduct);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newProduct = new Product();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getAllCategoryProducts() {
    this.authService.checkAuth();
    return this.db.collection(Models.categoryProduct.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newCategoryProduct = doc.data();
        newCategoryProduct.uid = doc.id;
        this.categoryProducts.push(newCategoryProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  edit(product: Product) {
    if (!product) {
      console.log('car is null');
      return;
    }
    this.authService.checkAuth();
    let editProduct = JSON.stringify(product);
    this.db.doc(Models.product.collectionName + '/' + product.uid).update(JSON.parse(editProduct)).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }
  
  remove(productId: string) {
    if (!productId) {
      console.log('productId is null');
      return;
    }
    this.authService.checkAuth();
    this.db.doc(Models.product.collectionName + '/' + productId).delete().then(
      (res) => {
        console.log(res);
        this.products = this.products.filter(x => x.uid !== productId);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.removeProduct = new Product();
        $('#remove-modal').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
