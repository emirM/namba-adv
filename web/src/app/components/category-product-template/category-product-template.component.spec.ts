import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryProductTemplateComponent } from './category-product-template.component';

describe('CategoryProductTemplateComponent', () => {
  let component: CategoryProductTemplateComponent;
  let fixture: ComponentFixture<CategoryProductTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryProductTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryProductTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
