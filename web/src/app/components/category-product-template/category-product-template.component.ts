// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';

// config
import { ApiConfig, NotifyConfig, Models } from '../../config';

// models
import { CategoryProduct } from '../../models/category-product';
import { ResponseApi } from '../../models/response-api';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { AuthService } from '../../services/auth/auth.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';

// constants
declare let $: any;

@Component({
  selector: 'app-category-product-template',
  templateUrl: './category-product-template.component.html',
  styleUrls: ['./category-product-template.component.css']
})
export class CategoryProductTemplateComponent implements OnInit, OnDestroy {
  newCategoryProduct= new CategoryProduct();
  categoryProducts = new Array<any>();
  removeCategoryProduct = new CategoryProduct();
  subscribes = new Array<any>();
  db = firebase.firestore();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private authService: AuthService,
    private unsubscribeService: UnsubscribeService
  ) { }

  ngOnInit() {
    this.getAll()
  }

  getAll() {
    return this.db.collection(Models.categoryProduct.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newCategoryProduct = doc.data();
          newCategoryProduct.uid = doc.id;
          this.categoryProducts.push(newCategoryProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  add() {
    let newCategoryProduct = JSON.stringify(this.newCategoryProduct);
    return this.db.collection(Models.categoryProduct.collectionName).add(JSON.parse(newCategoryProduct)).then(
      (res) => {
        console.log(res);
        let newAddCategoryProduct: CategoryProduct = this.newCategoryProduct;
        newAddCategoryProduct.uid = res.id;
        this.categoryProducts.push(newAddCategoryProduct);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newCategoryProduct = new CategoryProduct();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    })
  }

  edit(categoryProduct: CategoryProduct) {
    if (!categoryProduct) {
      console.log('categoryProduct is null');
      return;
    }

    this.db.doc(Models.categoryProduct.collectionName + '/' + categoryProduct.uid).update(categoryProduct).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }
  
  remove(categoryProductId: string) {
    if (!categoryProductId) {
      console.log('categoryProductId is null');
      return;
    }
    this.db.doc(Models.categoryProduct.collectionName + '/' + categoryProductId).delete().then(
      (res) => {
        console.log(res);
        this.categoryProducts = this.categoryProducts.filter(x => x.uid !== categoryProductId);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.removeCategoryProduct = new CategoryProduct();
        $('#remove-modal').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
