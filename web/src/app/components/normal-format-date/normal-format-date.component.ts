import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-normal-format-date',
  templateUrl: './normal-format-date.component.html',
  styleUrls: ['./normal-format-date.component.css']
})
export class NormalFormatDateComponent implements OnInit {
  @Input('date') date: Date;
  resultDate: string;
  constructor() { }

  ngOnInit() {
    this.toNormalDate(this.date);
  }

  toNormalDate(date: Date) {
    if (!date) return;
    date = new Date(date);
    this.resultDate = date.getDate().toString() + '.' + (date.getMonth()+1) + '.' + date.getFullYear().toString();
  }

}
