import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalFormatDateComponent } from './normal-format-date.component';

describe('NormalFormatDateComponent', () => {
  let component: NormalFormatDateComponent;
  let fixture: ComponentFixture<NormalFormatDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalFormatDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalFormatDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
