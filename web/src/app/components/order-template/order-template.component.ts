// core
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import * as firebase from 'firebase';
import * as _ from 'lodash';

// config
import { ApiConfig, NotifyConfig, Models, Times, RadioCoefficients } from '../../config';

// models
import { CategoryProduct } from '../../models/category-product';
import { Order } from '../../models/order';
import { Product } from '../../models/product';
import { ResponseApi } from '../../models/response-api';
import { Calc, CalcOption } from '../../models/calc';
import { TVChannel } from '../../models/tv-channel';
import { User } from '../../models/user';
import { OrderShablon } from '../../models/order-shablon';
import { Client } from '../../models/client';
import { Radio } from '../../models/radio';
import { ConsiderTime } from '../../models/consider-time';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { AuthService } from '../../services/auth/auth.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';
import { MyLocalStorageService } from '../../services/local-storage/local-storage.service';
import { TaxiProduct } from '../../models/taxi-product';

// constants
declare let $: any;

@Component({
  selector: 'app-order-template',
  templateUrl: './order-template.component.html',
  styleUrls: ['./order-template.component.css']
})
export class OrderTemplateComponent implements OnInit, OnDestroy {
  @Input('view-my-orders') isMyOrders: boolean;
  @Input('shablon-id') shablonId: string;

  subscribes = new Array<any>();
  categoryProducts = new Array<any>();
  products = new Array<Product>();
  radios = new Array<any>();
  allProducts = new Array<any>();
  newOrder = new Order();
  orders = new Array<any>();
  removeOrder = new Order();
  selectedCalc: any;
  selectedOrder = new Order();
  tvChannels = new Array<any>();
  total = 0;
  prevTotal = 0;
  totalWithNds = 0;
  withNds = false;
  currentUser = new User();
  newOrderShablon = new OrderShablon();
  newClient = new Client();
  isEdit = false;
  isRadioEdit = false;
  isShowCategories = false;
  times = Object.assign([], Times);
  coefficients = Object.assign([], RadioCoefficients);
  selectedRadio = new Radio();
  selectedCategory = new CategoryProduct();
  isCoefficient = false;
  weekDays = 0;
  weekendDays = 0;
  days = [];
  totalForRadioCreate = 0;
  considerTimes = new Array<ConsiderTime>();
  orderShablons = new Array<any>();
  isSaveShablonOrder = false;
  isPrintInvoice = false;
  isDetailInvoice = false;
  withDiscount: number;
  withFixSum: number;
  taxiProducts = new Array<any>();
  taxiPriceLists = new Array<any>();

  db = firebase.firestore();

  ru = {
    firstDayOfWeek: 0,
    dayNames: ["Воскресенье", "Понидельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
    dayNamesShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Субб"],
    dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Субб"],
    monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
    monthNamesShort: [ "Янв", "Фев", "Март", "Апр", "Май", "Июнь","Июль", "Авг", "Сен", "Окт", "Нояб", "Дек" ],
    today: 'Сегодня',
    clear: 'Очитсить'
  };

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private unsubscribeService: UnsubscribeService,
    private localStorageService: MyLocalStorageService
  ) { }

  ngOnInit() {
    this.currentUser = this.localStorageService.getUser();
    if (this.currentUser) {
      if (this.shablonId) this.getAllShablonById(this.shablonId);
      if (this.isMyOrders) this.getAllOrders();
      this.getAllCategories();
      this.newOrderInit();
      //this.getTvChannels();
      this.getAllProductsAll();
      this.getAllRadios();
      this.getAllTaxiProducts();
    }
  }

  getAllShablonById(shablonId: string) {
    console.log('getAllShablonById');
    return this.db.collection(Models.orderShablon.collectionName).doc(shablonId).get().then((doc) => {
      let newOrderShablon = doc.data();
      newOrderShablon.uid = doc.id;
      this.orders = newOrderShablon.orders;

      console.log('getAllShablonById: ', this.orders);
      this.orders.forEach((order) => {
        order.id = order.uid;
        order.uid = '';
        if(order.isRadio) {
          let startDate = new Date(order.startDate);
          order.startDate = startDate.getDate() + '.' + (startDate.getMonth()+1) + '.' + startDate.getFullYear();
          let endDate = new Date(order.endDate);
          order.endDate = endDate.getDate() + '.' + (endDate.getMonth()+1) + '.' + endDate.getFullYear();
          this.getRadioById(order);
        } else {
          this.getProductById(order);
        }
      });
      this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getRadioById(order: any) {
    console.log('getRadioById');
    if (!order || !order.radioId) return;
  
    this.db.collection(Models.radio.collectionName).doc(order.radioId).get().then((doc) => {
      console.log(doc.data());
      order.radio = doc.data();
      order.radio.uid = doc.id;

      // this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getProductById(order: any) {
    console.log('getProductById');
    if (!order || !order.productId) return;
    this.db.collection(Models.product.collectionName).doc(order.productId).get().then((doc) => {
      console.log(doc.data());
      order.product = doc.data();
      order.product.uid = doc.id;

      // this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  toPageCoefficient() {
    if (!this.selectedRadio || !this.selectedRadio.uid) {
      this.notifyService.showMessage('Выберите радио!', NotifyConfig.msgTypes.danger.styleName);
      return;
    }
    if (!this.selectedOrder.startDate || !this.selectedOrder.endDate) {
      this.notifyService.showMessage('Выберите Дату начало и Дату окончение!', NotifyConfig.msgTypes.danger.styleName);
      return;
    }
    this.isCoefficient = true;
  }

  checkDay(selectedOption: string, time: any, dayIndex: number) {
    console.log('checkDay', time, dayIndex);
    console.log('elem.checked = ' + time[selectedOption]);
    let price = this.selectedRadio.tariffs[time.option] || 0;
    if (dayIndex === 6 || dayIndex === 0) {
      price = this.selectedRadio.tariffs[time.weekendOption];
    }
    if (!this.selectedRadio || !this.selectedRadio.uid) {
      this.notifyService.showMessage('Выберите радио!', NotifyConfig.msgTypes.danger.styleName);
      time[selectedOption] = false;
      return;
    }
    if (!this.selectedOrder.startDate || !this.selectedOrder.endDate) {
      this.notifyService.showMessage('Выберите Дату начало и Дату окончение!', NotifyConfig.msgTypes.danger.styleName);
      return;
    }
    if (!time[selectedOption]) {
      this.considerTimes = this.considerTimes.filter(x => x.dayIndex !== dayIndex && x.price !== price);
      return this.consider(this.considerTimes, this.selectedOrder.coefficients);
    }
    let newConsiderTime = new ConsiderTime();
    newConsiderTime.dayIndex = dayIndex;
    newConsiderTime.price = price;
    newConsiderTime.time = time;
    this.considerTimes.push(newConsiderTime);
    this.consider(this.considerTimes, this.selectedOrder.coefficients);
  }

  consider(considerTimes: any, coefficients: any) {
    this.totalForRadioCreate = 0;
    considerTimes.forEach((considerTime) => {
      let days = this.days.filter(x => x === considerTime.dayIndex);
      if (days && days.length > 0) {
        this.totalForRadioCreate += days.length * Number(this.selectedOrder.duration) * Number(considerTime.price);
      }
    });
    if (coefficients && coefficients.length > 0) {
      coefficients.forEach((coefficient) => {
        if (coefficient.value) this.totalForRadioCreate = this.totalForRadioCreate * coefficient.value;
      });
    }
  }

  viewCoefficient(coefficient: any) {
    console.log('viewCoefficient');
    if (!coefficient.checked) {
      this.consider(this.considerTimes, this.selectedOrder.coefficients);
      return coefficient.value = 0;
    }
    coefficient.value = this.selectedRadio.coefficients[coefficient.option];
    this.consider(this.considerTimes, this.selectedOrder.coefficients);
  }

  viewTariff(time: any, status: boolean) {
    if (!status) return time.value = 0;
    time.value = this.selectedRadio.tariffs[time.option];
  }

  printInvoice() {
    window.print();
  }

  doInvoice() {
    this.isPrintInvoice=true;
    console.log('doInvoice');
    this.orders.forEach((order) => {
      if (order && order.isRadio) {
        order.radio = this.radios.filter(x => x.uid == order.radioId)[0];
        this.calcDaysAndWeekDays(order.startDate, order.endDate);
        if (order && order.considerTimes && order.considerTimes.length > 0) {
          this.consider(order.considerTimes, this.selectedOrder.coefficients);
          order.considerTimes.forEach((considerTime) => {
            let findTime = this.times.filter(x => x.option === considerTime.time.option)[0];
            if (findTime) {
              findTime = considerTime;
            }
          });
        }
      }
    });
  }

  calcDaysAndWeekDays(startDate: Date, endDate: Date) {
    startDate = new Date(startDate);
    endDate = new Date(endDate);

    this.weekendDays = 0;
    this.weekDays = 0;
    this.days = [];
    this.considerTimes = new Array<ConsiderTime>();
    console.log('calcDaysAndWeekDays', startDate, endDate);

    if (!startDate || !endDate) {
      console.log('not valid parameters');
      return;
    }
    if (startDate >= endDate) {
      console.log('Дата окончание должна быть больше Дата начало');
      return this.notifyService.showMessage('Дата окончание должна быть больше Дата начало!', NotifyConfig.msgTypes.danger.styleName);
    }

    while(startDate <= endDate) {
      this.days.push(startDate.getDay());
      if (startDate.getDay() === 6 || startDate.getDay() === 0) {
        this.weekendDays++; 
      } else {
        this.weekDays++;
      }
      startDate.setDate(startDate.getDate()+1);
    }
    console.log(this.weekendDays, this.weekDays);
  }

  newOrderInit() {
    console.log('newOrderInit');
    this.newOrder = new Order();
    this.newOrder.times = JSON.parse(JSON.stringify(this.times));
    this.newOrder.coefficients = JSON.parse(JSON.stringify(this.coefficients));
    this.newOrder.productId = null;
    this.newOrder.categoryId = null;
    this.selectedOrder = this.newOrder;
  }

  getAllOrders() {
    return this.db.collection(Models.order.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newOrder = doc.data();
          newOrder.uid = doc.id;
          this.orders.push(newOrder);
      });
      this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  setTotal(orders: Array<Order>) {
    this.total = 0;
    /*for (let i=0; i<orders.length; i++) {
      if (orders[i] && Number(orders[i].totalPrice)) {
        this.total += Number(orders[i].totalPrice);
      }
    }*/
    this.prevTotal = Math.ceil(_.sumBy(orders, function(o) { return o.totalPrice }));
    this.total = this.prevTotal;
    console.log('setTotal', this.total);
    this.doConsiderTotal();
  }

  doConsiderTotal() {
    console.log('doConsiderTotal');
    if (!this.total) {
      return;
    }
    this.total = this.prevTotal;
    if (this.withNds) this.total = Math.ceil(Number(this.total * 12 / 100) + this.total);
    if (this.withDiscount) this.total = Math.ceil(Number(this.total * this.withDiscount / 100) + this.total);
    if (this.withFixSum) this.total = this.total + this.withFixSum;
  }

  getAllProductsAll() {
    return this.db.collection(Models.product.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newProduct = doc.data();
          newProduct.uid = doc.id;
          this.allProducts.push(newProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getAllCategories() {
    return this.db.collection(Models.categoryProduct.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newCategoryProduct = doc.data();
        newCategoryProduct.uid = doc.id;
        this.categoryProducts.push(newCategoryProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getAllTaxiProducts() {
    console.log('getAllTaxiproducts');
    let newCategoryProduct = new CategoryProduct();
    newCategoryProduct.isTaxi = true;
    newCategoryProduct.name = 'Реклама на такси';
    newCategoryProduct.products = [];
    this.db.collection(Models.taxiProduct.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newTaxiProduct = doc.data();
        newTaxiProduct.uid = doc.id;
        this.taxiProducts.push(newTaxiProduct);
        newCategoryProduct.products.push(newTaxiProduct);
      });
      this.categoryProducts.push(newCategoryProduct);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getAllRadios() {
    return this.db.collection(Models.radio.collectionName).get().then(
      (querySnapshot) => {
        querySnapshot.docs.forEach((doc) => {
          let newRadio = doc.data();
          newRadio.uid = doc.id;
          this.radios.push(newRadio);
        });
        console.log('getAllRadios', this.radios);
      },
      (err) => {
        console.log(err);
      }
    )
  }

  selectRadio(radioId: string) {
    this.selectedRadio = new Radio();
    if (!radioId) {
      return console.log('radioId is null!');
    }
    let findRadio = this.radios.filter(x => x.uid === radioId)[0];
    if (findRadio) {
      this.selectedRadio = findRadio;
    }
  }

  calcRadio(order: Order) {
    console.log('calcRadio', order);
    if (!order) {
      console.log('order is null');
    }
    if(!order.radioId) {
      return this.notifyService.showMessage('Выберите радио', NotifyConfig.msgTypes.danger.styleName);
    }
    if (!order.startDate || !order.endDate) {
      return this.notifyService.showMessage('Введите дата начало и дата окончание!', NotifyConfig.msgTypes.info.styleName);
    }
  }

  setCategoryProducts(category: CategoryProduct) {
    console.log('setCategoryProducts');
    this.selectedCategory = new CategoryProduct();
    if (!category) {
      console.log('categoryId is null', category);
      return;
    }
    category.selected = !category.selected;
    if (!category.selected) return;
    this.selectedCategory = category;

    this.unselectedCategories(category.uid);
    if (category.isTaxi) {
      this.products = this.taxiProducts;
      return;
    }
    category.products = this.allProducts.filter(x => x.categoryId === category.uid);
    this.products = category.products;
  }

  unselectedCategories(categoryId: string) {
    this.categoryProducts.forEach((category) => {
      if (category.uid != categoryId) category.selected = false;
    });
  }

  selectCalcByProduct(product: Product) {
    console.log('selectCalcByProduct', this.selectedCalc);
    this.newOrder = new Order();
    this.selectedOrder =  this.newOrder;
    this.isEdit = false;
    this.db.doc(Models.calc.collectionName + '/' + product.calcId).get().then(
      (res) => {
        console.log(res.data());
        this.selectedCalc = res.data();
        product.calc = this.selectedCalc;
        this.newOrder.productId = product.uid;
        let findCategory = this.categoryProducts.filter(x => x.selected)[0];
        if (findCategory) this.newOrder.categoryId = findCategory.uid;
        $('#calc-product-modal').modal('show');
        this.selectedOrder =  this.newOrder;
        this.getTvChannelsByProductId(product.uid);
      }
    ).catch(
      (err) => {
        console.log(err);
      }
    );
  }

  selectCalcTaxiByProduct(taxiProductUid: string) {
    this.selectedOrder.taxiProductId = taxiProductUid;
    this.selectedOrder.isTaxi = true;

    $('#calc-taxi-modal').modal('show');
    this.taxiPriceLists = [];
    this.db.collection(Models.taxiPriceList.collectionName).where('taxiProductId', '==', taxiProductUid).get().then(
      (querySnapshot) => {
        //this.tvChannels = [];
        querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newTaxiPriceList = doc.data();
          newTaxiPriceList.uid = doc.id;
          this.taxiPriceLists.push(newTaxiPriceList);
        });
        this.considerTaxi(this.selectedOrder);
      }
    ).catch(
      (err) => {
        console.log(err);
      }
    );
  }

  considerTaxi(order: Order) {
    console.log('considerTaxi');
    if (!this.taxiPriceLists || this.taxiPriceLists.length < 1 || !order) return;
    order.monthCount = order.monthCount || 1;
    order.carsCount = order.carsCount || 1;
    let findPriceList = this.taxiPriceLists.filter(x => x.monthCount === order.monthCount)[0];
    if (!findPriceList) {
      findPriceList = this.taxiPriceLists.filter(x => x.monthCount === 1)[0];
    }
    order.priceCar = findPriceList.priceCar || 0;
    order.totalPrice = Number(order.priceCar) * Number(order.carsCount) * Number(order.monthCount);
  }

  getTvChannelsByProductId(productId: string) {
    return this.db.collection(Models.tvChannel.collectionName).where('productId', '==', productId ).get().then(
      (querySnapshot) => {
        //this.tvChannels = [];
          querySnapshot.docs.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            console.dir(doc.data());
            let newTvChannel = doc.data();
            newTvChannel.uid = doc.id;
            this.tvChannels.push(newTvChannel);
        });
      }
    ).catch(
      (err) => {
        console.log(err);
      }
    );
  }

  changeProduct(productId: string) {
    if (!productId) return console.log('productId is null!');
    let findProduct = this.products.filter(x => x.uid == productId)[0];
    if (!findProduct) return console.log('product not found!');
    if (!findProduct.calc) {
      return this.db.doc(Models.calc.collectionName + '/' + findProduct.calcId).get().then(
        (res) => {
          console.log(res.data());
          this.selectedCalc = res.data();
          findProduct.calc = this.selectedCalc;
          this.selectedOrder.calc = this.selectedCalc;
          this.selectedOrder.productId = productId;
          this.selectedOrder.product = this.allProducts.filter(x => x.uid === productId)[0];
        }
      ).catch(
        (err) => {
          console.log(err);
        }
      );
    }
    this.selectedCalc = findProduct.calc;
    this.selectedOrder.calc = this.selectedCalc;
    this.selectedOrder.productId = productId;
    this.selectedOrder.product = this.allProducts.filter(x => x.uid === productId)[0];
  }

  openCalcOfProduct(selectedCalc: Calc) {
    console.log('openCalcOfProduct', selectedCalc);
    $('#calc-product-modal').modal('show');
    if (!selectedCalc || !selectedCalc.name) { // если не выбрана калькулятор или отсуствует по какой то причине тогда все печально мен
      return;
    };

    if (!this.selectedCalc.selectedTvChannelId) { // если не выбрана телеканал то расчет останавливается к сожалению
      return;
    }

    for (let i=0; i<selectedCalc.options.length; i++) {
      if (selectedCalc.options[i].ref) {
        selectedCalc.options[i].value = selectedCalc.options[i].value || [];
        continue;
      }

      if (selectedCalc.options[i].formulas.length > 0) {
        selectedCalc.options[i].modifadeFormulas = selectedCalc.options[i].formulas.slice();
        console.log('selectedCalc.options[i].formulas', selectedCalc.options[i].formulas);
        console.log('selectedCalc.options[i].modifadeFormulas', selectedCalc.options[i].modifadeFormulas);

        for (let k=0; k<selectedCalc.options[i].modifadeFormulas.length; k++) {
          let findOption = selectedCalc.options.filter(x => x.name === selectedCalc.options[i].modifadeFormulas[k])[0];
          console.log('findOption: ', findOption);
          if (!findOption) {
            continue;
          }

          if (Number(findOption.value)) {
            selectedCalc.options[i].modifadeFormulas[k] = Number(findOption.value);
            continue;
          }

          if (findOption.value && findOption.value.length > 0) {
            let findVal = findOption.value.filter(x => x.name === findOption.selectedValueName)[0] || findOption.value[0];
            findOption.selectedValueName = findVal.name;
            console.log('findVal:', findVal);
            if (findVal && findVal.value || findVal.price) {
              selectedCalc.options[i].modifadeFormulas[k] = findVal.value || findVal.price;
              continue;
            }
          }
        }

        console.log('selectedCalc.options', selectedCalc.options);
        for (let j=0; j<selectedCalc.options[i].modifadeFormulas.length; j++) {
          if (Number(selectedCalc.options[i].modifadeFormulas[j])) {
            continue;
          }

          if (selectedCalc.options[i].modifadeFormulas[j] !== '*' && selectedCalc.options[i].modifadeFormulas[j] !== '/'
            && selectedCalc.options[i].modifadeFormulas[j] !== '+' && selectedCalc.options[i].modifadeFormulas[j] !== '-') {
            continue;
          }

          let firstNumber = Number(selectedCalc.options[i].modifadeFormulas[j-1]);
          let secondNumber = Number(selectedCalc.options[i].modifadeFormulas[j+1]);

          if (!firstNumber && firstNumber !== 0) {
            continue;
          }

          if (!secondNumber && secondNumber !== 0) {
            continue;
          }

          switch(selectedCalc.options[i].modifadeFormulas[j]) {
            case '*':
              selectedCalc.options[i].modifadeFormulas[j+1] = firstNumber * secondNumber;
              selectedCalc.options[i].value = selectedCalc.options[i].modifadeFormulas[j+1];
              break;
            case '/':
              selectedCalc.options[i].modifadeFormulas[j+1] = firstNumber / secondNumber;
              selectedCalc.options[i].value = selectedCalc.options[i].modifadeFormulas[j+1];
              break;
            case '+':
              selectedCalc.options[i].modifadeFormulas[j+1] = firstNumber + secondNumber;
              selectedCalc.options[i].value = selectedCalc.options[i].modifadeFormulas[j+1];
              break;
            case '-':
              selectedCalc.options[i].modifadeFormulas[j+1] = firstNumber - secondNumber;
              selectedCalc.options[i].value = selectedCalc.options[i].modifadeFormulas[j+1];
              break;
          } 
        }
      }
    }
  }

  getTvChannels() {
    return this.db.collection(Models.tvChannel.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newTvChannel = doc.data();
          newTvChannel.uid = doc.id;
          this.tvChannels.push(newTvChannel);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  doSelectTvChannel() {
    let selectedTvChannel: TVChannel = this.tvChannels.filter(x => x.uid === this.selectedCalc.selectedTvChannelId)[0];
    if (selectedTvChannel) {
      for (let i=0; i<this.selectedCalc.options.length; i++) {
        if (this.selectedCalc.options[i].ref === "Tariff") {
          this.selectedCalc.options[i].value = selectedTvChannel.tariffs;
        } else if (this.selectedCalc.options[i].ref === "Сoefficient") {
          this.selectedCalc.options[i].value = selectedTvChannel.coefficients;
        }
      }
      this.openCalcOfProduct(this.selectedCalc);
    }
  }

  getByModelName(option: CalcOption) {
    if (!option || !option.ref) {
      console.log('modelName is null');
      return option.value = [];
    }
    this.subscribes.push(
      this.masterQueryService.get(ApiConfig.model.getByModelName.url.replace(':modelName', option.ref), false).subscribe(
        (res: ResponseApi) => {
          if (!res.success) {
            return option.value = [];
          }
          return option.value = res.data.docs || []
        },
        (e) => {
          console.log(e);
          return option.value = [];
        }
      )
    )
  }

  showRadioOrder(order: Order) {
    console.log('showRadioOrder', order);
    this.getRadioById(order);
    this.selectedOrder = order;
    this.selectedRadio = order.radio;
    
    this.calcDaysAndWeekDays(order.startDate, order.endDate);
    if (order && order.considerTimes && order.considerTimes.length > 0) {
      this.consider(order.considerTimes, order.coefficients);
      order.considerTimes.forEach((considerTime) => {
        let findTime = this.times.filter(x => x.option === considerTime.time.option)[0];
        if (findTime) {
          findTime = considerTime;
        }
      });
      $('#radio-modal').modal('show');
    }
  }

  saveRadioOrder(order: Order) {
    console.log('saveRadioOrder', order);
    if (!order) {
      return this.notifyService.showMessage('Заполните данные!', NotifyConfig.msgTypes.danger.styleName);
    }
    if (!order.radioId) {
      return this.notifyService.showMessage('Выберите радио!', NotifyConfig.msgTypes.danger.styleName)
    }
    if (!order.startDate || !order.endDate) {
      return this.notifyService.showMessage('Выберите Дата начало и Дата окончание', NotifyConfig.msgTypes.danger.styleName);
    }
    if (!order.duration) {
      return this.notifyService.showMessage('Установите длительность ролика', NotifyConfig.msgTypes.danger.styleName);
    }
    if (this.considerTimes.length < 1) {
      return this.notifyService.showMessage('Выберите время на таблице', NotifyConfig.msgTypes.danger.styleName);
    }
    order.isRadio = true;
    console.log('saveRadioOrder', order);
    if(!order.considerTimes || order.considerTimes.length < 1) order.considerTimes = this.considerTimes;
    if (!order.totalPrice) order.totalPrice = this.totalForRadioCreate;
    //if (!order.coefficients || order.coefficients.length < 1) order.coefficients = this.coefficients;
    this.save(order);
  }

  save(order: Order) {
    console.log('save', order);
    if (!order) {
      return this.notifyService.showMessage('Заполните данные!', NotifyConfig.msgTypes.success.styleName);
    }
    order.categoryId = this.selectedCategory.uid;
    order.category = this.selectedCategory;
    order.category.products = [];
    if(order.isTaxi) {
      order.product = this.products.filter(x => x.uid == order.taxiProductId)[0];
      order.productId = order.product.uid || '';
      order.coefficients = [];
      order.considerTimes = [];
      order.times = [];
    }
    if (this.newClient && this.newClient.uid) order.clientId = this.newClient.uid;
    if (this.currentUser.uid) order.userId = this.currentUser.uid;
    if (!order.uid) {
      return this.addNewOrder(order);
    }
    this.editOrder(order);
  }

  addNewOrder(order: Order) {
    console.log('addNewOrder', order);
    //order.coefficients = this.coefficients.filter(x => x.checked);
    let neworder = JSON.stringify(order);
    return this.db.collection(Models.order.collectionName).add(JSON.parse(neworder)).then(
      (res) => {
        console.log(res);
        order.uid = res.id;
        order.category = this.selectedCategory;
        if (!order.isRadio && !order.isTaxi) {
          order.product = this.allProducts.filter(x => x.uid === order.productId)[0];
          if (order.calc && order.calc.options && order.calc.options.length > 0) {
            let findOption = order.calc.options.filter(x => x.formulas && x.formulas.length > 0)[0];
            if (findOption && Number(findOption.value)) {
              order.totalPrice = Number(findOption.value);
            }
          }
        }
        
        $('#radio-modal').modal('hide');
        if (!this.isSaveShablonOrder) this.orders.push(order);
        this.setTotal(this.orders);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
        order = new Order();
        //this.selectedTvChannelId = '';
        //this.products = new Array<Product>();
        this.clearOrder();
        this.coefficients = Object.assign([], RadioCoefficients);
        this.times = Object.assign([], Times);
        this.isShowCategories = false;
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  clearOrder() {
    this.selectedCalc = new Calc();
    this.selectedOrder = new Order();
    this.newOrder = new Order();
    this.days = [];
    this.weekDays = 0;
    this.weekendDays = 0;
    //this.total = 0;
    //this.totalWithNds = 0;
    this.totalForRadioCreate = 0;

    this.times = Object.assign([], Times);
    this.coefficients = Object.assign([], RadioCoefficients);
    this.newOrderInit();
  }

  editOrder(order: Order) {
    if (!order.uid) return console.log('order is not created!');
    let neworder = JSON.stringify(order);
    this.db.doc(Models.order.collectionName + '/' + order.uid).update(JSON.parse(neworder)).then(
      (res) => {
        console.log(res);

        if (!order.isRadio && order.calc && order.calc.options && order.calc.options.length > 0) {
          let findOption = order.calc.options.filter(x => x.formulas && x.formulas.length > 0)[0];
          if (findOption && Number(findOption.value)) {
            order.totalPrice = Number(findOption.value);
          }
        }
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
        $('#radio-modal').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  saveClient(client: Client) {
    console.log('add', client);
    if (
      !client.uid) {
      return this.addNewClient(client);
    }
    this.editClient(client);
  }

  addNewClient(client: Client) {
    let newclient = JSON.stringify(client);
    return this.db.collection(Models.client.collectionName).add(JSON.parse(newclient)).then(
      (res) => {
        console.log(res);
        client.uid = res.id;
        this.newClient = client;
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        $('#add-client-modal').modal('hide');
        this.setClientIdForSavedOrders(this.newClient.uid);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  editClient(client: Client) {
    if (!client.uid) return console.log('client uid is null');
    let newclient = JSON.stringify(client);
    this.db.doc(Models.client.collectionName + '/' + client.uid).update(JSON.parse(newclient)).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  setClientIdForSavedOrders(clientId: string) {
    let savedOrders = this.orders.filter(x => x.uid || !x.clientId);
    if (!savedOrders || savedOrders.length < 1 || !clientId) return;
    savedOrders.forEach((order) => {
      order.clientId = clientId;
      this.editOrder(order);
    });
  }

  saveShablon(orderShablon: OrderShablon) {
    console.log('add new shablon', orderShablon);
    if (!orderShablon || !orderShablon.name) {
      return this.notifyService.showMessage('Введите наименование шаблона', NotifyConfig.msgTypes.warning.styleName);
    }
    if (!this.orders || this.orders.length < 1) {
      $('#save-shablon-modal').modal('hide');
      return this.notifyService.showMessage('Введите заказ', NotifyConfig.msgTypes.warning.styleName);
    }
    orderShablon.orders = this.orders || [];
    orderShablon.userId = this.currentUser.uid;
    let newOrderShablon = JSON.stringify(orderShablon);
    return this.db.collection(Models.orderShablon.collectionName).add(JSON.parse(newOrderShablon)).then(
      (res) => {
        console.log(res);
        $('#save-shablon-modal').modal('hide');
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }
  
  remove(order: Order) {
    console.log('remove order', order);
    if (!order) {
      console.log('order.uid is null');
      return;
    }
    if (!order.uid) {
      this.orders = this.orders.filter(x => x.id !== order.id);
      $('#remove-modal').modal('hide');
      return;
    }
    this.db.doc(Models.order.collectionName + '/' + order.uid).delete().then(
      (res) => {
        console.log(res);
        this.orders = this.orders.filter(x => x.uid !== order.uid);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.removeOrder = new Order();
        this.selectedCalc = new Calc();
        //this.selectedTvChannelId = '';
        $('#remove-modal').modal('hide');
        this.setTotal(this.orders);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
