// cores
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

// config
import { Times, RadioCoefficients, NotifyConfig, Models } from '../../config';

// models
import { Radio } from '../../models/radio';

// services
import { NotifyService } from '../../services/notify/notify.service';
declare let $: any;

@Component({
  selector: 'app-radio-template',
  templateUrl: './radio-template.component.html',
  styleUrls: ['./radio-template.component.css']
})
export class RadioTemplateComponent implements OnInit {
  times = Times;
  radioCoefficients = RadioCoefficients;
  newRadio = new Radio();
  radios = new Array<any>();
  db = firebase.firestore();
  removeRadio = new Radio();

  constructor(
    private notifyService: NotifyService
  ) { }

  ngOnInit() {
    this.getAllRadios();
  }

  getAllRadios() {
    return this.db.collection(Models.radio.collectionName).get().then(
      (querySnapshot) => {
        querySnapshot.docs.forEach((doc) => {
          let newRadio = doc.data();
          newRadio.uid = doc.id;
          this.radios.push(newRadio);
        });
      },
      (err) => {
        console.log(err);
      }
    )
  }

  save(radio: Radio) {
    console.log('add radio');
    if (!radio || !radio.name) {
      this.notifyService.showMessage('Введите наименование радио!', NotifyConfig.msgTypes.danger.styleName);
      return;
    }
    if(!radio.uid) {
      return this.add(radio);
    }
    this.edit(radio);
  }

  add(radio: Radio) {
    console.log('add radio', radio);
    if (!radio || !radio.name) {
      this.notifyService.showMessage('Введите наименование радио!', NotifyConfig.msgTypes.danger.styleName);
      return;
    }
    let tariffElems = document.getElementsByClassName('new_' + radio.name);
    if (!tariffElems || tariffElems.length < 0) return;

    let newRadio = JSON.stringify(radio);
    return this.db.collection(Models.radio.collectionName).add(JSON.parse(newRadio)).then(
      (res) => {
        console.log(res);
        radio.uid = res.id;
        this.radios.push(radio);
        this.notifyService.showMessage('Успешно создано', NotifyConfig.msgTypes.success.styleName);
        this.newRadio = new Radio();
      },
      (err) => {
        console.log(err);
        this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName)
      }
    )
  }

  edit(radio: Radio) {
    console.log('edit', radio);
    if (!radio || !radio.uid) return console.log('is radio not created!');
    let newRadio = JSON.stringify(radio);
    this.db.doc(Models.radio.collectionName + '/' + radio.uid).update(JSON.parse(newRadio)).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено', NotifyConfig.msgTypes.success.styleName);
      },
      (err) => {
        console.log(err);
        this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
      }
    )
  }

  remove(radioId: string) {
    if (!radioId) {
      console.log('radioId is null!');
      return;
    }
    this.db.doc(Models.radio.collectionName + '/' + radioId).delete().then(
      (res) => {
        console.log(res);
        this.radios = this.radios.filter(x => x.uid !== radioId);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.info.styleName);
        this.removeRadio = new Radio();
        $('#remove-modal').modal('hide');
      }
    )
  }

}
