// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';

// config
import { ApiConfig, NotifyConfig, Models } from '../../config';

// models
import { ResponseApi } from '../../models/response-api';
import { Role } from '../../models/role';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';
import { MyLocalStorageService } from '../../services/local-storage/local-storage.service';
import { AuthService } from '../../services/auth/auth.service';
import { Operation } from '../../models/operation';

declare const $: any;

@Component({
  selector: 'app-role-template',
  templateUrl: './role-template.component.html',
  styleUrls: ['./role-template.component.css']
})
export class RoleTemplateComponent implements OnInit, OnDestroy {

  subscribes = new Array<any>();
  roles = new Array<any>();
  newRole = new Role();
  selectedRole = new Role();
  removeRole = new Role();
  db = firebase.firestore();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private unsubscribeService: UnsubscribeService,
    private localStorageService: MyLocalStorageService,
    private authService: AuthService
  ) {

  }

  ngOnInit() {
    this.getAllRoles();
  }

  changeSelectedRole(role: Role) {
    console.log('changeSelectedRole', role);
    this.selectedRole = role;
  }

  getAllRoles() {
    console.log('getAllRoles');
    return this.db.collection(Models.role.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let role = doc.data();
          role.uid = doc.id;
          this.roles.push(role);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    })
  }

  add() {
    console.log('add:', this.newRole);
    if (!this.newRole || !this.newRole.name || !this.newRole.description) {
      return this.notifyService.showMessage('Заполните все поля', NotifyConfig.msgTypes.danger.styleName);
    }

    let models = Object.keys(Models);
    if (!models || models.length < 1) {
        console.log('models is null');
        return;
    }
    models.forEach((key) => {
      let operation = new Operation();
      operation.modelLabel = Models[key].modelName;
      operation.name = Models[key].name;
      this.newRole.operations.push(operation);
    });
    let newOperationString = JSON.stringify(this.newRole.operations);

    return this.db.collection(Models.role.collectionName).add({
      'name': this.newRole.name,
      'description' : this.newRole.description,
      'operations': JSON.parse(newOperationString)
    }).then(
      (res) => {
        console.log(res);
        let newAddRole: Role = this.newRole;
        newAddRole.uid = res.id;
        this.roles.push(newAddRole);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newRole = new Role();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    })
  }

  edit(role: Role) {
    if (!role || !role.uid) {
      console.log('role is null');
      return;
    }

    this.db.doc(Models.role.collectionName + '/' + role.uid).update(role).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
        $('#operation').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    })
  }

  remove(roleId: string) {
    if (!roleId) {
      return;
    }
    this.db.doc(Models.role.collectionName + '/' + roleId).delete().then(
      (res) => {
        console.log(res);
        this.roles = this.roles.filter(x => x.uid !== roleId);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.removeRole = new Role();
        $('#remove-modal').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    })
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
