// core
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

// config
import { ApiConfig, NotifyConfig } from '../../config';

// models
import { ResponseApi } from '../../models/response-api';
import { Role } from '../../models/role';
import { Operation } from '../../models/operation';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';
import { MyLocalStorageService } from '../../services/local-storage/local-storage.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-operation-template',
  templateUrl: './operation-template.component.html',
  styleUrls: ['./operation-template.component.css']
})
export class OperationTemplateComponent implements OnInit, OnDestroy {
  
  @Input('roleId') roleId: string;
  subscribes = new Array<any>();
  operations = new Array<Operation>();
  newOperation = new Operation();

  searchResultRoles = new Array<Role>();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private unsubscribeService: UnsubscribeService,
    private localStorageService: MyLocalStorageService,
    private authService: AuthService
  ) {
  }
  
  ngOnInit() {
    this.getAll();
  }

  getAll() {
    if (!this.roleId) {
      console.log('roleId is null');
      return;
    }
    this.subscribes.push(
      this.masterQueryService.get(ApiConfig.role.getOperations.url.replace(':id', this.roleId), true).subscribe(
        (res: ResponseApi) => {
          this.authService.checkAuth();
          if (!res.success) {
            return this.notifyService.showMessage(res.msg || 'Что то пошло не так.', NotifyConfig.msgTypes.warning.styleName);
          }
          this.operations = res.data.docs || [];
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage('Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
        }
      )
    );
  }

  edit(operation: Operation) {
    if (!operation) {
      console.log('role is null');
      return;
    }
    console.log('edit', operation);
    operation.roleId = operation.roleId._id
    this.subscribes.push(
      this.masterQueryService.post(ApiConfig.operation.edit.url, operation, true).subscribe(
        (res: ResponseApi) => {
          this.authService.checkAuth();
          if (!res.success) {
            return this.notifyService.showMessage(res.msg || 'Что то пошло не так.', NotifyConfig.msgTypes.warning.styleName);
          }
          this.notifyService.showMessage(res.msg || 'Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage('Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
        }
      )
    );
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
