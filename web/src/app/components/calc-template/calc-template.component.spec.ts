import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcTemplateComponent } from './calc-template.component';

describe('CalcTemplateComponent', () => {
  let component: CalcTemplateComponent;
  let fixture: ComponentFixture<CalcTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
