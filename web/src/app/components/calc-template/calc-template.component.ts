// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';

// config
import { ApiConfig, NotifyConfig, Models } from '../../config';

// models
import { Calc, CalcOption } from '../../models/calc';
import { RefTable } from '../../models/refTable';
import { ResponseApi } from '../../models/response-api';

// services
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { NotifyService } from '../../services/notify/notify.service';
import { AuthService } from '../../services/auth/auth.service';
import { UnsubscribeService } from '../../services/unsubscribe/unsubscribe.service';

// constants
declare let $: any;

@Component({
  selector: 'app-calc-template',
  templateUrl: './calc-template.component.html',
  styleUrls: ['./calc-template.component.css']
})
export class CalcTemplateComponent implements OnInit, OnDestroy {
  newOption = new CalcOption();
  newOptions = new Array<CalcOption>();
  newCalc = new Calc();
  calcs = new Array<any>();
  refTables = new Array<RefTable>();
  removeCalc = new Calc();
  prevNumber: any;
  showFormulaCalInCalc = new Calc();
  selectedOption = new CalcOption();
  db = firebase.firestore();

  subscribes = new Array<any>();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private authService: AuthService,
    private unsubscribeService: UnsubscribeService
  ) { }


  ngOnInit() {
    let newRef = new RefTable();
    newRef.name = 'Не выбрано';
    this.refTables.push(newRef);
    this.getAll();
  }

  setNumber(number: number, option: CalcOption, options: Array<CalcOption>) {
    console.log('setNumber', number, option);
    let lastFormula = option.formulas[option.formulas.length - 1];
    if (!lastFormula && lastFormula !== 0) {
      return option.formulas.push(number);
    }
    if (!Number(lastFormula) && lastFormula !== 0) {
      if (lastFormula === '.') {
        return option.formulas[option.formulas.length - 1] += number.toString();
      }
      
      let findOption = options.filter(x => x.name === lastFormula)[0];
      if (findOption) {
        return;
      }
      return option.formulas.push(number);
    }
    if (lastFormula === 0) {
      return option.formulas[option.formulas.length - 1] = number;
    }
    option.formulas[option.formulas.length - 1] += number.toString();
  }

  setDot(dot: string, option: CalcOption) {
    console.log('setDot', option);
    let lastFormula = option.formulas[option.formulas.length - 1];
    if (!lastFormula) {
      return;
    }
    if (!Number(lastFormula)) {
      return;
    }
    let findDot = lastFormula.split(dot);
    if (findDot.length > 1) return;
    option.formulas[option.formulas.length - 1] += dot.toString();
  }

  clearCalc(option: CalcOption) {
    option.formulas = [];
  }

  doOperation(operator: string, option: CalcOption, options: Array<CalcOption>) {
    console.log('doOperation');
    let lastFormula = option.formulas[option.formulas.length - 1];
    if (!lastFormula) {
      return console.log('введите числа');
    }
    if (!Number(lastFormula)) {
      let findOption = options.filter(x => x.name === lastFormula)[0];
      if (findOption) {
        return option.formulas.push(operator);
      }
      return option.formulas[option.formulas.length - 1] = operator;
    }
    option.formulas.push(operator);
  }

  getAll() {
          
    let newRefModel = new RefTable();
    newRefModel.name = 'Тариф';
    newRefModel.modelName = 'Tariff';
    this.refTables.push(newRefModel);

    let newRefModelCoef = new RefTable();
    newRefModelCoef.name = 'Коэфициент';
    newRefModelCoef.modelName = 'Сoefficient';
    this.refTables.push(newRefModelCoef);

    let newRefModelTv = new RefTable();
    newRefModelTv.name = 'Телеканал';
    newRefModelTv.modelName = 'TVChannel';
    this.refTables.push(newRefModelTv);
    return this.db.collection(Models.calc.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
          console.log(`${doc.id} => ${doc.data()}`);
          console.dir(doc.data());
          let newCalc = doc.data();
          newCalc.uid = doc.id;
          this.calcs.push(newCalc);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  add() {
    this.showFormulaCalInCalc = new Calc();
    this.selectedOption = new CalcOption();
    console.log('add', this.newCalc);
    if (!this.newCalc) {
      return this.notifyService.showMessage('Запоните данные.', NotifyConfig.msgTypes.warning.styleName);
    }
    let findDublicate = this.calcs.filter(x => x.name === this.newCalc.name);
    if (findDublicate.length > 0) {
      console.log('dublicate');
      return this.notifyService.showMessage('Калькулятор с таким имененем уже существует!', NotifyConfig.msgTypes.danger.styleName);
    }

    let newClalc= JSON.stringify(this.newCalc);
    return this.db.collection(Models.calc.collectionName).add(JSON.parse(newClalc)).then(
      (res) => {
        console.log(res);
        this.newCalc.uid = res.id;
        this.calcs.push(this.newCalc);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newCalc = new Calc();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  showFormulaCalc(option: CalcOption, calc: Calc) {
    option.showFormulaCalc = !option.showFormulaCalc;
    this.showFormulaCalInCalc = option.showFormulaCalc ? calc : new Calc();
    this.selectedOption = option;
  }

  clearShowFormulaCalc(options: Array<CalcOption>, optionName: string) {
    if (!options || options.length < 1) {
      return;
    }
    options.forEach((option) => {
      if (option.name !== optionName) {
        option.showFormulaCalc = false;
      }
    });

  }

  chooseField (optionName: string) {
    console.log('chooseField');
    if (this.selectedOption.formulas.length < 1) {
      return this.selectedOption.formulas.push(optionName);
    }
    let operator = this.selectedOption.formulas[this.selectedOption.formulas.length-1];
    if (!operator || Number(operator)) {
      return;
    }
    this.selectedOption.formulas.push(optionName);
  }

  addOption(option: CalcOption) {
    console.log('addOption', option);
    if (!this.newCalc.name) {
      return this.notifyService.showMessage('Введите наименование калькулятора!', NotifyConfig.msgTypes.danger.styleName);
    }
    if (!option.name) {
      return this.notifyService.showMessage('Введите наименование!', NotifyConfig.msgTypes.danger.styleName);
    }
    this.showFormulaCalInCalc = new Calc();
    this.selectedOption = new CalcOption();
    this.newCalc.options = this.newCalc.options || new Array<CalcOption>();
    let findDublicate = this.newCalc.options.filter(x => x.name === option.name);
    if (findDublicate.length > 0) {
      console.log('dublicate');
      return this.notifyService.showMessage('Поле с таким имененем уже существует!', NotifyConfig.msgTypes.danger.styleName);
    }
    this.newCalc.options.push(option);
    this.newOption = new CalcOption();
  }

  addOptionInEdit(option: CalcOption, calc: Calc) {
    calc.options = calc.options || new Array<CalcOption>(); 
    calc.options = calc.options || new Array<CalcOption>();
    let findDublicate = calc.options.filter(x => x.name === option.name);
    if (findDublicate.length > 0) {
      console.log('dublicate');
      return this.notifyService.showMessage('Поле с таким имененем уже существует!', NotifyConfig.msgTypes.danger.styleName);
    }
    calc.options.push(option);
    this.newOption = new CalcOption();
  }

  removeOption(option: CalcOption) {
    console.log('removeOption', option);
    this.newCalc.options = this.newCalc.options.filter(x => x.name !== option.name);
  }

  removeOptionInCalc(option: CalcOption, calc: Calc) {
    console.log('removeOptionInCalc', option);
    calc.options = calc.options.filter(x => x.name !== option.name);
  }

  edit(calc: Calc) {
    this.showFormulaCalInCalc = new Calc();
    this.selectedOption = new CalcOption();
    if (!calc) {
      return this.notifyService.showMessage('Запоните данные.', NotifyConfig.msgTypes.warning.styleName);
    }
    let findDublicate = this.calcs.filter(x => x.name === calc.name && x.uid !== calc.uid);
    if (findDublicate.length > 0) {
      console.log('dublicate');
      return this.notifyService.showMessage('Калькулятор с таким имененем уже существует!', NotifyConfig.msgTypes.danger.styleName);
    }

    this.authService.checkAuth();
    let editCalc = JSON.stringify(calc);
    this.db.doc(Models.calc.collectionName + '/' + calc.uid).update(JSON.parse(editCalc)).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }
  
  remove(calcId: string) {
    if (!calcId) {
      console.log('calcId is null');
      return;
    }

    this.db.doc(Models.calc.collectionName + '/' + calcId).delete().then(
      (res) => {
        console.log(res);
        this.calcs = this.calcs.filter(x => x.uid !== calcId);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.removeCalc = new Calc();
        $('#remove-modal').modal('toggle');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
