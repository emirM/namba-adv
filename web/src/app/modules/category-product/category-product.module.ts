// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { CategoryProductRoutingModule, routedComponents } from './category-product.routing.module';
import { CategoryProductComponent } from './category-product.component';
import { CategoryProductTemplateComponent } from '../../components/category-product-template/category-product-template.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    CategoryProductRoutingModule,
    SharedModule
  ],
  exports: [
    CategoryProductTemplateComponent
  ],
  declarations: [ CategoryProductComponent, CategoryProductTemplateComponent, routedComponents ],
  providers: [AuthService],
})
export class CategoryProductModule { }
