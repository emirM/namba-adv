// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { CategoryProductWelcomeComponent } from '../../pages/category-product-welcome/category-product-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: CategoryProductWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryProductRoutingModule { }

export const routedComponents = [
    CategoryProductWelcomeComponent
];
