// cores
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// modules
import { SharedModule } from '../shared/shared.module';
import { TaxiPriceListRoutingModule, routedComponents } from './taxi-price-list.routing.module';

@NgModule({
  imports: [
    RouterModule,
    TaxiPriceListRoutingModule,
    SharedModule
  ],
  exports: [],
  declarations: [
    routedComponents
  ]
})
export class TaxiPriceListModule { }
