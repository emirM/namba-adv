import { TaxiPriceListModule } from './taxi-price-list.module';

describe('TaxiPriceListModule', () => {
  let taxiPriceListModule: TaxiPriceListModule;

  beforeEach(() => {
    taxiPriceListModule = new TaxiPriceListModule();
  });

  it('should create an instance', () => {
    expect(taxiPriceListModule).toBeTruthy();
  });
});
