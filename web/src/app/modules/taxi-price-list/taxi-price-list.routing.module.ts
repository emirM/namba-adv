// cores
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

// components
import { TaxiPriceListWelcomeComponent } from '../../pages/taxi-price-list-welcome/taxi-price-list-welcome.component';

const routes: Routes = [
    {
        path: '',
        component: TaxiPriceListWelcomeComponent
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class TaxiPriceListRoutingModule {}

export const routedComponents = [
    TaxiPriceListWelcomeComponent
];