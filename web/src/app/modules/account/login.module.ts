// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { LoginRoutingModule, routedComponents } from './login.routing.module';
import { LoginComponent } from './login.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    LoginRoutingModule,
    SharedModule
  ],
  exports: [],
  declarations: [LoginComponent, routedComponents],
  providers: [AuthService],
})

export class LoginModule {
}
