// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { RegisterComponent } from '../../pages/account/register/register.component';
import { LoginWelcomeComponent } from '../../pages/account/login-welcome/login-welcome.component';
import { ProfileWelcomeComponent } from '../../pages/account/profile-welcome/profile-welcome.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginWelcomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'profile',
    component: ProfileWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule { }

export const routedComponents = [
  RegisterComponent,
  LoginWelcomeComponent,
  ProfileWelcomeComponent
];
