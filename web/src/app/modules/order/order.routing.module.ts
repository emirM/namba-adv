// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { OrderWelcomeComponent } from '../../pages/order-welcome/order-welcome.component';
import { OrdersWelcomeComponent } from '../../pages/orders-welcome/orders-welcome.component';
import { OrderShablonWelcomeComponent } from '../../pages/order-shablon-welcome/order-shablon-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: OrderWelcomeComponent
  },
  {
    path: 'shablon/:shablonId',
    component: OrderWelcomeComponent
  },
  {
    path: 'my',
    component: OrdersWelcomeComponent
  },
  {
    path: 'shablons',
    component: OrderShablonWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderRoutingModule { }

export const routedComponents = [
  OrderWelcomeComponent,
  OrdersWelcomeComponent,
  OrderShablonWelcomeComponent
];
