// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { OrderRoutingModule, routedComponents } from './order.routing.module';
import { OrderComponent } from './order.component';
import { OrderTemplateComponent } from '../../components/order-template/order-template.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    OrderRoutingModule,
    SharedModule
  ],
  exports: [
    OrderTemplateComponent
  ],
  declarations: [ OrderComponent, OrderTemplateComponent, routedComponents ],
  providers: [AuthService],
})
export class OrderModule { }
