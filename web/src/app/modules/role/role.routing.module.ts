// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { RoleWelcomeComponent } from '../../pages/role-welcome/role-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: RoleWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleRoutingModule { }

export const routedComponents = [
    RoleWelcomeComponent
];
