// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// services
import { AuthService } from '../../services/auth/auth.service';

// component
import { RoleRoutingModule, routedComponents } from './role.routing.module';
import { RoleComponent } from './role.component';
import { RoleTemplateComponent } from '../../components/role-template/role-template.component';
import { OperationTemplateComponent } from '../../components/operation-template/operation-template.component';

@NgModule({
  imports: [
    RouterModule,
    RoleRoutingModule,
    SharedModule
  ],
  exports: [
    RoleTemplateComponent,
    OperationTemplateComponent
  ],
  declarations: [
    RoleComponent,
    RoleTemplateComponent,
    OperationTemplateComponent,
    routedComponents
  ],
  providers: [AuthService],
})
export class RoleModule { }
