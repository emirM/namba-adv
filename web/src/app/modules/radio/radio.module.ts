// cores
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// modules
import { SharedModule } from '../shared/shared.module';
import { RadioRoutingModule, routedComponents } from './radio.routing.module';

// components
import { RadioTemplateComponent } from '../../components/radio-template/radio-template.component';

@NgModule({
  imports: [
    RouterModule,
    RadioRoutingModule,
    SharedModule
  ],
  exports: [
    RadioTemplateComponent
  ],
  declarations: [
    RadioTemplateComponent,
    routedComponents
  ]
})
export class RadioModule { }
