// cores
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { RadioWelcomeComponent } from '../../pages/radio-welcome/radio-welcome.component';

const routes: Routes = [
    {
        path: '',
        component: RadioWelcomeComponent
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class RadioRoutingModule {}

export const routedComponents = [
    RadioWelcomeComponent
];