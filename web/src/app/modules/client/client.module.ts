// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { ClientRoutingModule, routedComponents } from './client.routing.module';
import { ClientComponent } from './client.component';
import { ClientTemplateComponent } from '../../components/client-template/client-template.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    ClientRoutingModule,
    SharedModule
  ],
  exports: [
    ClientTemplateComponent
  ],
  declarations: [ ClientComponent, ClientTemplateComponent, routedComponents ],
  providers: [AuthService],
})
export class ClientModule { }
