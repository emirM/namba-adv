// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { ClientWelcomeComponent } from '../../pages/client-welcome/client-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: ClientWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientRoutingModule { }

export const routedComponents = [
    ClientWelcomeComponent
];
