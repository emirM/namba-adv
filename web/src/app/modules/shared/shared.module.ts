// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';

// components
import { NavbarHeaderComponent } from '../../components/navbar-header/navbar-header.component';
import { NormalFormatDateComponent } from '../../components/normal-format-date/normal-format-date.component';

// services
import { LocalStorageService } from 'angular-2-local-storage';
import { AuthService } from '../../services/auth/auth.service';
import { DateFormatService } from '../../services/date-format/date-format.service';
import { MasterQueryService } from '../../services/master-query/master-query.service';
import { HandleErrorService } from '../../services/handle-error/handle-error.service';

@NgModule({
 imports: [
    RouterModule,
    HttpModule,
    CommonModule,
    FormsModule,
    AutoCompleteModule,
    CalendarModule
 ],
 exports: [
    HttpModule,
    RouterModule,
    CommonModule,
    FormsModule,
    NavbarHeaderComponent,
    AutoCompleteModule,
    CalendarModule,
    NormalFormatDateComponent
 ],
 declarations: [
    NavbarHeaderComponent,
    NormalFormatDateComponent
 ],
 providers: [
    LocalStorageService,
    AuthService,
    DateFormatService,
    MasterQueryService,
    HandleErrorService
 ],
})
export class SharedModule { }
