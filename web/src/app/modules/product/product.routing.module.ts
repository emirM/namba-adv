// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { ProductWelcomeComponent } from '../../pages/product-welcome/product-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: ProductWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRoutingModule { }

export const routedComponents = [
    ProductWelcomeComponent
];
