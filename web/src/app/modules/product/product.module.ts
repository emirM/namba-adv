// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { ProductRoutingModule, routedComponents } from './product.routing.module';
import { ProductComponent } from './product.component';
import { ProductTemplateComponent } from '../../components/product-template/product-template.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    ProductRoutingModule,
    SharedModule
  ],
  exports: [
    ProductTemplateComponent
  ],
  declarations: [ ProductComponent, ProductTemplateComponent, routedComponents ],
  providers: [AuthService],
})
export class ProductModule { }
