// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { TvChannelWelcomeComponent } from '../../pages/tv-channel-welcome/tv-channel-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: TvChannelWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TvChannelRoutingModule { }

export const routedComponents = [
    TvChannelWelcomeComponent
];
