// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { TvChannelRoutingModule, routedComponents } from './tv-channel.routing.module';
import { TvChannelComponent } from './tv-channel.component';
import { TvChannelTemplateComponent } from '../../components/tv-channel-template/tv-channel-template.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    TvChannelRoutingModule,
    SharedModule
  ],
  exports: [
    TvChannelTemplateComponent
  ],
  declarations: [ TvChannelComponent, TvChannelTemplateComponent, routedComponents ],
  providers: [AuthService],
})
export class TVChannelModule { }
