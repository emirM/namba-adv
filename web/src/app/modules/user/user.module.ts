// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// services
import { AuthService } from '../../services/auth/auth.service';

// component
import { UserRoutingModule, routedComponents } from './user.routing.module';
import { UserComponent } from './user.component';
import { UserTemplateComponent } from '../../components/user-template/user-template.component';

@NgModule({
  imports: [
    RouterModule,
    UserRoutingModule,
    SharedModule
  ],
  exports: [
    UserTemplateComponent
  ],
  declarations: [
    UserComponent,
    UserTemplateComponent,
    routedComponents
  ],
  providers: [AuthService],
})
export class UserModule { }
