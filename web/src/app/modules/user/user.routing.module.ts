// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { UserWelcomeComponent } from '../../pages/user-welcome/user-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: UserWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule { }

export const routedComponents = [
    UserWelcomeComponent
];
