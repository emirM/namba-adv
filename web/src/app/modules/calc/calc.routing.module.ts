// core
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { CalcWelcomeComponent } from '../../pages/calc-welcome/calc-welcome.component';

const routes: Routes = [
  {
    path: '',
    component: CalcWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalcRoutingModule { }

export const routedComponents = [
    CalcWelcomeComponent
];
