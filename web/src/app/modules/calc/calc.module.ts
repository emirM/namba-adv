// core
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// module
import { SharedModule } from '../shared/shared.module';

// component
import { CalcRoutingModule, routedComponents } from './calc.routing.module';
import { CalcComponent } from './calc.component';
import { CalcTemplateComponent } from '../../components/calc-template/calc-template.component';
import { AuthService } from '../../services/auth/auth.service';

@NgModule({
  imports: [
    RouterModule,
    CalcRoutingModule,
    SharedModule
  ],
  exports: [
    CalcTemplateComponent
  ],
  declarations: [ CalcComponent, CalcTemplateComponent, routedComponents ],
  providers: [AuthService],
})
export class CalcModule { }
