// cores
import { Component, OnInit } from '@angular/core';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-product-welcome',
  templateUrl: './product-welcome.component.html',
  styleUrls: ['./product-welcome.component.css']
})
export class ProductWelcomeComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
