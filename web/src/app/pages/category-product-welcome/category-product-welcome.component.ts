// cores
import { Component, OnInit } from '@angular/core';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-category-product-welcome',
  templateUrl: './category-product-welcome.component.html',
  styleUrls: ['./category-product-welcome.component.css']
})
export class CategoryProductWelcomeComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
