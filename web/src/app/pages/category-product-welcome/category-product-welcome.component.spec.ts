import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryProductWelcomeComponent } from './category-product-welcome.component';

describe('CategoryProductWelcomeComponent', () => {
  let component: CategoryProductWelcomeComponent;
  let fixture: ComponentFixture<CategoryProductWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryProductWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryProductWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
