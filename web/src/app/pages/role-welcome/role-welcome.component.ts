// cores
import { Component, OnInit } from '@angular/core';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-role-welcome',
  templateUrl: './role-welcome.component.html',
  styleUrls: ['./role-welcome.component.css']
})
export class RoleWelcomeComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
