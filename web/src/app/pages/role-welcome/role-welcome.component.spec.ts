import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleWelcomeComponent } from './role-welcome.component';

describe('RoleWelcomeComponent', () => {
  let component: RoleWelcomeComponent;
  let fixture: ComponentFixture<RoleWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
