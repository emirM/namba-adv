// core
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

// config
import { ApiConfig, NotifyConfig } from '../../../config';

// models
import { User } from '../../../models/user';
import { ResponseApi } from '../../../models/response-api';

// services
import { NotifyService } from '../../../services/notify/notify.service';
import { AuthService } from '../../../services/auth/auth.service';
import { MyLocalStorageService } from '../../../services/local-storage/local-storage.service';
import { MasterQueryService } from '../../../services/master-query/master-query.service';


@Component({
  selector: 'app-login-welcome',
  templateUrl: './login-welcome.component.html',
  styleUrls: ['./login-welcome.component.css']
})
export class LoginWelcomeComponent implements OnInit {
  subscribes = new Array<any>();
  newUser = new User();

  constructor(
    private notifyService: NotifyService,
    private authService: AuthService,
    private localStorageService: MyLocalStorageService,
    private router: Router,
    private masterQueryService: MasterQueryService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToProfile();
  }

  doLogin() {
    console.log('doLogin');
    if (!this.newUser || !this.newUser.email || !this.newUser.password) {
      this.notifyService.showMessage('Логин и Пароль обьязательные поля.', NotifyConfig.msgTypes.warning.styleName);
      return;
    }
    firebase.auth().signInWithEmailAndPassword(this.newUser.email, this.newUser.password)
    .then((res) => {
      console.log(res);
      let user = new User();
      user.uid = res.user.uid;
      user.email = res.user.email;
      this.localStorageService.setUser(user);
      this.notifyService.showMessage('Успешно!', NotifyConfig.msgTypes.success.styleName);
      //console.log(firebase.auth().currentUser);
      this.router.navigate(['/order']);
    })
    .catch(function(error) {
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;
      console.log(error);
      if (errorCode === 'auth/user-not-found') errorMessage = 'Пользователь с таким логином не найдено!';
      this.notifyService.showMessage(errorMessage, NotifyConfig.msgTypes.danger.styleName);
    });
  }

}
