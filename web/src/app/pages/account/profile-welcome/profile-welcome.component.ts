// core
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

// models
import { User } from '../../../models/user';

// services
import { MyLocalStorageService } from '../../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-profile-welcome',
  templateUrl: './profile-welcome.component.html',
  styleUrls: ['./profile-welcome.component.css']
})
export class ProfileWelcomeComponent implements OnInit {
  currentUser = new User();

  constructor(
    private localStorageService: MyLocalStorageService
  ) { }

  ngOnInit() {
    let user = firebase.auth().currentUser;
    this.currentUser = this.localStorageService.getUser();
    this.currentUser.email = user.email;
  }

}
