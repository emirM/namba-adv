// core
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

// config
import { ApiConfig, NotifyConfig } from '../../../config';

// models
import { ResponseApi } from '../../../models/response-api';
import { User } from '../../../models/user';

// services
import { MasterQueryService } from '../../../services/master-query/master-query.service';
import { NotifyService } from '../../../services/notify/notify.service';
import { MyLocalStorageService } from '../../../services/local-storage/local-storage.service';
import { UnsubscribeService } from '../../../services/unsubscribe/unsubscribe.service';

import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  subscribes = new Array<any>();
  newUser = new User();
  db = firebase.firestore();

  constructor(
    private masterQueryService: MasterQueryService,
    private notifyService: NotifyService,
    private localStorageService: MyLocalStorageService,
    private router: Router,
    private unsubscribeService: UnsubscribeService
  ) { }

  ngOnInit() {
    console.log('currentUser', firebase.auth().currentUser);
  }

  doRegister() {
    console.log('doRegister');
    if (!this.newUser) {
      return this.notifyService.showMessage('Заполните все данные.', NotifyConfig.msgTypes.danger.styleName);
    }
    firebase.auth().createUserWithEmailAndPassword(this.newUser.email, this.newUser.password)
    .then((res) => {
      console.log(res);
      this.newUser.uid = res.user.uid;
      this.db.collection('users').doc(this.newUser.uid).set({
        'name': this.newUser.name,
        'email': this.newUser.email,
        'uid': this.newUser.uid
      }).then(
        (res) => {
          console.log(res);
          this.localStorageService.setUser(this.newUser);
          this.router.navigate(['/order']);
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage(err, NotifyConfig.msgTypes.danger.styleName);
        }
      )
    }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
    /*
    this.subscribes.push(
      this.masterQueryService.post(ApiConfig.user.register.url, this.newUser, false).subscribe(
        (res: ResponseApi) => {
          console.log('doRegister', res);
          if (!res.success) {
            return this.notifyService.showMessage(res.msg || 'Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
          }
          this.localStorageService.setToken(res.token);
          this.localStorageService.setUser(res.data);
          let user: User = res.data;
          return this.router.navigate(['/tariff']);
        },
        (err) => {
          console.log(err);
          this.notifyService.showMessage('Что то пошло не так.', NotifyConfig.msgTypes.danger.styleName);
        }
      )
    );*/
  }

  ngOnDestroy() {
    this.unsubscribeService.unsubscribings(this.subscribes);
  }

}
