import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioWelcomeComponent } from './radio-welcome.component';

describe('RadioWelcomeComponent', () => {
  let component: RadioWelcomeComponent;
  let fixture: ComponentFixture<RadioWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
