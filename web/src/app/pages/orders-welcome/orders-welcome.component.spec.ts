import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersWelcomeComponent } from './orders-welcome.component';

describe('OrdersWelcomeComponent', () => {
  let component: OrdersWelcomeComponent;
  let fixture: ComponentFixture<OrdersWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
