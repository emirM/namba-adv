// cores
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

// models
import { TaxiPriceList } from '../../models/taxi-price-list';
import { TaxiProduct } from '../../models/taxi-product';

// config
import { NotifyConfig, Models } from '../../config';

// services
import { NotifyService } from '../../services/notify/notify.service';

// consts
declare var $: any;

@Component({
  selector: 'app-taxi-price-list-welcome',
  templateUrl: './taxi-price-list-welcome.component.html',
  styleUrls: ['./taxi-price-list-welcome.component.css']
})
export class TaxiPriceListWelcomeComponent implements OnInit {
  newTaxiPriceList = new TaxiPriceList();
  newTaxiProduct = new TaxiProduct();
  taxiProducts = new Array<any>();
  taxiPriceLists = new Array<any>();
  selectedTaxiPriceList = new TaxiPriceList();
  db = firebase.firestore();

  constructor(
    private notifyService: NotifyService
  ) { }

  ngOnInit() {
    this.getAll();
    this.getAllTaxiProducts();
  }

  getAll() {
    console.log('getAll');
    return this.db.collection(Models.taxiPriceList.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newTaxiPriceList = doc.data();
        newTaxiPriceList.uid = doc.id;
        this.taxiPriceLists.push(newTaxiPriceList);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getAllTaxiProducts() {
    console.log('getAllTaxiproducts');
    return this.db.collection(Models.taxiProduct.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newTaxiProduct = doc.data();
        newTaxiProduct.uid = doc.id;
        this.taxiProducts.push(newTaxiProduct);
      });
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  add(taxiPriceList: TaxiPriceList) {
    let newTaxiPriceList = JSON.stringify(taxiPriceList);
    return this.db.collection(Models.taxiPriceList.collectionName).add(JSON.parse(newTaxiPriceList)).then(
      (res) => {
        console.log(res);;
        taxiPriceList.uid = res.id;
        this.taxiPriceLists.push(taxiPriceList);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newTaxiPriceList = new TaxiPriceList();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  edit(taxiPriceList: TaxiPriceList) {
    if (!taxiPriceList || !taxiPriceList.uid) {
      console.log('taxiPriceList is null');
      return;
    }
    this.db.doc(Models.taxiPriceList.collectionName + '/' + taxiPriceList.uid).update(taxiPriceList).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  remove(uid: string) {
    if (!uid) {
      console.log('uid is null');
      return;
    }
    this.db.doc(Models.taxiPriceList.collectionName + '/' + uid).delete().then(
      (res) => {
        console.log(res);
        this.taxiPriceLists = this.taxiPriceLists.filter(x => x.uid !== uid);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.selectedTaxiPriceList = new TaxiPriceList();
        $('#remove-modal').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  addTaxiProduct(taxiProduct: TaxiProduct) {
    let newTaxiProduct = JSON.stringify(taxiProduct);
    return this.db.collection(Models.taxiProduct.collectionName).add(JSON.parse(newTaxiProduct)).then(
      (res) => {
        console.log(res);;
        taxiProduct.uid = res.id;
        this.taxiProducts.push(taxiProduct);
        this.notifyService.showMessage('Успешно создано!', NotifyConfig.msgTypes.success.styleName);
        this.newTaxiProduct = new TaxiProduct();
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  editTaxiProduct(taxiProduct: TaxiProduct) {
    if (!taxiProduct || !taxiProduct.uid) {
      console.log('taxiProduct is null');
      return;
    }
    this.db.doc(Models.taxiProduct.collectionName + '/' + taxiProduct.uid).update(taxiProduct).then(
      (res) => {
        console.log(res);
        this.notifyService.showMessage('Успешно сохранено!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  removeTaxiProduct(uid: string) {
    if (!uid) {
      console.log('uid is null');
      return;
    }
    this.db.doc(Models.taxiProduct.collectionName + '/' + uid).delete().then(
      (res) => {
        console.log(res);
        this.taxiProducts = this.taxiProducts.filter(x => x.uid !== uid);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

}
