import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxiPriceListWelcomeComponent } from './taxi-price-list-welcome.component';

describe('TaxiPriceListWelcomeComponent', () => {
  let component: TaxiPriceListWelcomeComponent;
  let fixture: ComponentFixture<TaxiPriceListWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxiPriceListWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxiPriceListWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
