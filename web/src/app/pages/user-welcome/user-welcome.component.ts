// cores
import { Component, OnInit } from '@angular/core';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-user-welcome',
  templateUrl: './user-welcome.component.html',
  styleUrls: ['./user-welcome.component.css']
})
export class UserWelcomeComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
