import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvChannelWelcomeComponent } from './tv-channel-welcome.component';

describe('TvChannelWelcomeComponent', () => {
  let component: TvChannelWelcomeComponent;
  let fixture: ComponentFixture<TvChannelWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvChannelWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvChannelWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
