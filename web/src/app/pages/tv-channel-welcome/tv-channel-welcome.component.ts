// cores
import { Component, OnInit } from '@angular/core';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-tv-channel-welcome',
  templateUrl: './tv-channel-welcome.component.html',
  styleUrls: ['./tv-channel-welcome.component.css']
})
export class TvChannelWelcomeComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
