import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcWelcomeComponent } from './calc-welcome.component';

describe('CalcWelcomeComponent', () => {
  let component: CalcWelcomeComponent;
  let fixture: ComponentFixture<CalcWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
