// cores
import { Component, OnInit } from '@angular/core';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-calc-welcome',
  templateUrl: './calc-welcome.component.html',
  styleUrls: ['./calc-welcome.component.css']
})
export class CalcWelcomeComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
