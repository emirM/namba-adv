import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderShablonWelcomeComponent } from './order-shablon-welcome.component';

describe('OrderShablonWelcomeComponent', () => {
  let component: OrderShablonWelcomeComponent;
  let fixture: ComponentFixture<OrderShablonWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderShablonWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderShablonWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
