// cores
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

// config
import { Models, NotifyConfig } from '../../config';

// models
import { OrderShablon } from '../../models/order-shablon';

// services
import { NotifyService } from '../../services/notify/notify.service';

// constants
declare var $: any;

@Component({
  selector: 'app-order-shablon-welcome',
  templateUrl: './order-shablon-welcome.component.html',
  styleUrls: ['./order-shablon-welcome.component.css']
})
export class OrderShablonWelcomeComponent implements OnInit {
  orderShablons = new Array<any>();
  db = firebase.firestore();
  selectedOrderShablon = new OrderShablon();

  constructor(
    private notifyService: NotifyService
  ) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    return this.db.collection(Models.orderShablon.collectionName).get().then((querySnapshot) => {
      console.log(querySnapshot);
      querySnapshot.docs.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.dir(doc.data());
        let newOrderShablon = doc.data();
        newOrderShablon.uid = doc.id;
        this.orderShablons.push(newOrderShablon);
        this.orderShablons.forEach((orderShablon) => {
          orderShablon.orders.forEach((order) => {
            if(order.isRadio) {
              this.getRadioById(order);
            } else {
              this.getProductById(order);
            }
          });
        });
      });
      // this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getRadioById(order: any) {
    console.log('getRadioById');
    if (!order || !order.radioId) return;
  
    this.db.collection(Models.radio.collectionName).doc(order.radioId).get().then((doc) => {
      console.log(doc.data());
      order.radio = doc.data();
      order.radio.id = doc.id;

      // this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  getProductById(order: any) {
    console.log('getProductById');
    if (!order || !order.productId) return;
    this.db.collection(Models.product.collectionName).doc(order.productId).get().then((doc) => {
      console.log(doc.data());
      order.product = doc.data();
      order.product.id = doc.id;

      // this.setTotal(this.orders);
    }).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

  remove(uid: string) {
    if (!uid) {
      console.log('uid is null');
      return;
    }
    this.db.doc(Models.orderShablon.collectionName + '/' + uid).delete().then(
      (res) => {
        this.orderShablons = this.orderShablons.filter(x => x.uid !== uid);
        this.notifyService.showMessage('Успешно удалена!', NotifyConfig.msgTypes.success.styleName);
        this.selectedOrderShablon = new OrderShablon();
        $('#remove-modal').modal('hide');
      }
    ).catch((err) => {
      console.log(err);
      this.notifyService.showMessage('Что то пошло не так!', NotifyConfig.msgTypes.danger.styleName);
    });
  }

}
