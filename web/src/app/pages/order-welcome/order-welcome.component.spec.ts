import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderWelcomeComponent } from './order-welcome.component';

describe('OrderWelcomeComponent', () => {
  let component: OrderWelcomeComponent;
  let fixture: ComponentFixture<OrderWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
