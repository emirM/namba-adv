
// cores
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// services
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-order-welcome',
  templateUrl: './order-welcome.component.html',
  styleUrls: ['./order-welcome.component.css']
})
export class OrderWelcomeComponent implements OnInit {
  shablonId = '';
  constructor(
    private authService: AuthService,
    private route: ActivatedRoute
  ) {
    this.shablonId = this.route.snapshot.params['shablonId'];
    console.log('this.shablonId: ', this.shablonId );
  }

  ngOnInit() {
    this.authService.isCheckAuthRedirectToLogin();
  }

}
