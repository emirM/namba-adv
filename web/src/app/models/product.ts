export class Product {
    name: string;
    _id: string;
    uid: string;
    categoryId: any;
    calcId: any;
    calc: any;
    createdAt: string;
    updatedAt: string;
}
