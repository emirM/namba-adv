export class TaxiPriceList {
    uid: string;
    name: string;
    monthCount: number;
    carsCount: number;
    priceCar: number;
    taxiProductId: string;
}