export class Calc {
    _id: string;
    uid: string;
    name: string;
    selectedTvChannelId = '';
    options: Array<CalcOption>;
}

export class CalcOption {
    name: string;
    value: any;
    modifadeFormulas = [];
    formulas = [];
    ref: string;
    selectedValueName = '';
    selectedValueVal: any;
    showFormulaCalc = false;
}
