export class Operation {
    _id: string;
    name: string;
    modelLabel: string;
    isRead = true;
    isAdd = false;
    isRemove = false;
    isEdit = false;
    roleId: any;
    createdAt: any;
    updatedAt: any;    
}