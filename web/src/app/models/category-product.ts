export class CategoryProduct {
    name: string;
    _id: string;
    uid: string;
    products: any;
    selected = false;
    isRadio = false;
    isTaxi = false;
    createdAt: string;
    updatedAt: string;
}
