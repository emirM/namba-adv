import { Coefficient } from './coefficient';
import { Tariff } from './tariff';

export class TVChannel {
    _id: string;
    uid: string;
    name: string;
    coefficients: Array<Coefficient>;
    tariffs: Array<Tariff>;
    productId: any;

    createdAt: any;
    updatedAt: any;
}