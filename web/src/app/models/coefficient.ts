export class Coefficient {
    _id: string;
    name: string;
    value: number;
    createdAt: any;
    updatedAt: any;
}