export class ResponseApi {
    success: boolean;
    data: any;
    msg: string;
    docs: Array<any>;
    token: string;
    limit: number;
    page: number;
    pages: number;
    total: number;
  }
  