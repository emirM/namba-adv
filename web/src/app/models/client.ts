export class Client {
    name: string;
    address: string;
    inn: any;
    bankDetails: string;
    _id: string;
    uid: string;
}