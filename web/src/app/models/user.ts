import { Operation } from './operation';

export class User {
    _id: string;
    id: string;
    uid: string;
    name: string;
    email: string;
    isAdmin = false;
    roleId: string;
    password: string;
    operations = new Array<Operation>();
    createdAt: string;
    updatedAt: string;
}
