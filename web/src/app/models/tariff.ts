export class Tariff {
    _id: string;
    name: string;
    price = 0;
    createdAt: string;
    updatedAt: string;
}
