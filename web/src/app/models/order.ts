import { Times, RadioCoefficients } from '../config';

export class Order {
    _id: string;
    id: string;
    categoryId: any;
    category: any;
    productId: any;
    product: any;
    userId: any;
    calc: any;
    clientId: any;
    isRadio = false;
    radioId: any;
    radio: any;
    duration = 0; // длительность ролика если это радио
    startDate: any;
    endDate: any;
    uid: string;
    considerTimes = new Array<any>();
    totalPrice = 0;
    tarriffs = new Array<any>();
    coefficients = []; // copy object;
    times = []; // copy object

    isTaxi = false;
    taxiProductId: string;
    monthCount: number;
    carsCount: number;
    priceCar: number;

    createdAt: any;
    updatedAt: any;
}