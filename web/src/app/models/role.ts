import { Operation } from '../models/operation';

export class Role {
    _id: string;
    uid: string;
    name: string;
    description: string;
    operations = new Array<Operation>();
    createdAt: string;
    updatedAt: string;
}