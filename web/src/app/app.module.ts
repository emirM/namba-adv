// core
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import * as firebase from 'firebase';

// services
import { HandleErrorService } from './services/handle-error/handle-error.service';
import { UnsubscribeService } from './services/unsubscribe/unsubscribe.service';
import { MyLocalStorageService } from './services/local-storage/local-storage.service';
import { AuthService } from './services/auth/auth.service';
import { NotifyService } from './services/notify/notify.service';
import { MasterQueryService } from './services/master-query/master-query.service';

// localstorage
import { LocalStorageModule, LocalStorageService } from 'angular-2-local-storage';

// routing
import { AppRoutingModule } from './app.routing';

// components
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';

firebase.initializeApp(environment.firebase);
const firestore = firebase.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
firestore.settings(settings);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    LocalStorageModule.withConfig({
      prefix: 'app-root',
      storageType: 'localStorage'
    })
  ],
  providers: [
    HandleErrorService,
    UnsubscribeService,
    MyLocalStorageService,
    LocalStorageService,
    AuthService,
    NotifyService,
    MasterQueryService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
