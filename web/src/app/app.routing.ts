// core
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// components
import { ProfileWelcomeComponent } from './pages/account/profile-welcome/profile-welcome.component'

const APP_ROUTES: Routes = [
  {
    path: '',
    loadChildren: 'src/app/modules/order/order.module#OrderModule'
  },
  {
    path: 'account',
    loadChildren: 'src/app/modules/account/login.module#LoginModule'
  },
  {
    path: 'profile',
    component: ProfileWelcomeComponent
  },
  {
    path: 'category-product',
    loadChildren: 'src/app/modules/category-product/category-product.module#CategoryProductModule'
  },
  {
    path: 'products',
    loadChildren: 'src/app/modules/product/product.module#ProductModule'
  },
  {
    path: 'role',
    loadChildren: 'src/app/modules/role/role.module#RoleModule'
  },
  {
    path: 'user',
    loadChildren: 'src/app/modules/user/user.module#UserModule'
  },
  {
    path: 'tv-channel',
    loadChildren: 'src/app/modules/tv-channel/tv-channel.module#TVChannelModule'
  },
  {
    path: 'order',
    loadChildren: 'src/app/modules/order/order.module#OrderModule'
  },
  {
    path: 'client',
    loadChildren: 'src/app/modules/client/client.module#ClientModule'
  },
  {
    path: 'calc',
    loadChildren: 'src/app/modules/calc/calc.module#CalcModule'
  },
  {
    path: 'radio',
    loadChildren: 'src/app/modules/radio/radio.module#RadioModule'
  },
  {
    path: 'taxi',
    loadChildren: 'src/app/modules/taxi-price-list/taxi-price-list.module#TaxiPriceListModule'
  },
  {
    path: '**',
    loadChildren: 'src/app/modules/order/order.module#OrderModule'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(APP_ROUTES) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {
}
